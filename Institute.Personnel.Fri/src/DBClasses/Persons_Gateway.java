/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBClasses;

import ObjectClasses.*;
import java.util.*;

/**
 *
 * @author justiceforall88
 */
public class Persons_Gateway implements Persons{
    private static Map Persons = new HashMap<Integer, Person>();
    public int select(Person p){
        Set keys = Persons.keySet();
        for (Iterator it = keys.iterator(); it.hasNext();) {
            int key = (int) it.next();
            if (Persons.get(key).equals(p)){
                return key;
            }
        }
        return -1;
    }
    public Person select(int id){
        return (Person) Persons.get(id);
    }
    public List<Person> select_params(Person p){
        List <Person> findPerson = new ArrayList<>();
        Person pd_tmp;
        boolean []ncase = new boolean [7];
        boolean []ecase = new boolean [7];
        boolean fcase;
        for (int i=0;i<Persons.size();i++){
            pd_tmp = (Person) Persons.get(i);    
            fcase = true;    
            ncase[0] = (p.getFIO()!= null);
            ncase[1] = (p.getPost()!= null);
            ncase[2] = (p.getSubdiv()!= null);
            ncase[3] = (p.getScRank()!= null);
            ncase[4] = (p.getScGrade() != null);
            ncase[5] = (p.getFiles()!= null);
            ncase[6] = (p.getContracts()!= null);   
            
            ecase[0] = (p.getFIO() == null ? pd_tmp.getFIO() == null : Arrays.equals(p.getFIO(), pd_tmp.getFIO()));
            ecase[1] = (p.getPost() == null ? pd_tmp.getPost() == null : p.getPost().equals(pd_tmp.getPost()));
            ecase[2] = (p.getSubdiv() == null ? pd_tmp.getSubdiv() == null : p.getSubdiv().equals(pd_tmp.getSubdiv()));
            ecase[3] = (p.getScRank() == null ? pd_tmp.getScRank() == null : p.getScRank().equals(pd_tmp.getScRank()));
            ecase[4] = (p.getScGrade() == null ? pd_tmp.getScGrade() == null : p.getScGrade().equals(pd_tmp.getScGrade()));
            ecase[5] = (p.getFiles()== null ? pd_tmp.getFiles()== null : Arrays.equals(p.getFiles(), pd_tmp.getFiles()));
            ecase[6] = (p.getContracts()== null ? pd_tmp.getContracts()== null : Arrays.equals(p.getContracts(), pd_tmp.getContracts()));
            for (int j=0; j<7; j++){          
                if (ncase[j]){
                    fcase &= ecase[j];
                }            
            }
            if (fcase){
                findPerson.add(pd_tmp);
            }
        }
        return findPerson;
    }
    public boolean insert(int id, Person p){
        if (Persons.get(id)==null){
            Persons.put(id,p);
            return true;
        } 
        else
            return false;
    }
    public boolean update(int id, Person p){
        if (Persons.get(id)!=null){
            Persons.replace(id, p);
            return true;
        }
        else
            return false;
    }
    public boolean delete(int id){
        if (Persons.get(id)!=null){
            Persons.remove(id);
            return true;
        }
        else
            return false;
    }
    public Map<Integer, Person> getAll(){
        return Persons;
    }
    public void clear(){
        Persons.clear();
    }
}
