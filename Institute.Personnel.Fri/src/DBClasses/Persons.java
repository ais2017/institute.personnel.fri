/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBClasses;

import ObjectClasses.*;
import java.util.*;

/**
 *
 * @author justiceforall88
 */
public interface Persons {
    int select(Person p);
    Person select(int id);
    List<Person> select_params(Person p);
    boolean insert(int id, Person p);
    boolean update(int id, Person p);
    boolean delete(int id);
    Map<Integer, Person> getAll();
    void clear();
}
