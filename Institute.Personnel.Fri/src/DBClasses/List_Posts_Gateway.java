/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBClasses;

import ObjectClasses.*;
import java.util.*;

/**
 *
 * @author justiceforall88
 */
public class List_Posts_Gateway implements List_Posts{
    private static List<String> Posts = new ArrayList<>(Arrays.asList("Ректор", "Заместитель ректора",
        "Руководитель структурного подразделения","Начальник отдела аспирантуры",
        "Советник при ректорате", "Ассистент", "Преподаватель", "Старший преподаватель",
        "Доцент", "Ученый секретарь совета учреждения", "Помощник ректора",
    "Профессор", "Заведующий кафедрой", "Заместитель заведующего кафедрой", "Декан факультета",
    "Заместитель декана факультета", "Диспетчер факультета", "Тьютор", 
    "Специалист по учебно-методической работе", "Учебный мастер"));
    public int select(String post){
        if (post!=null){
            for (int i=0; i<Posts.size(); i++){
                if ((Posts.get(i)!=null)&&(Posts.get(i).equals(post)))
                    return i;
            }
        }
        return -1;
    }
    public boolean insert(String post){
        Set<String> set = new HashSet<String>(Posts);
        if (!set.contains(post)){
            Posts.add(post);
            return true;
        }
        else
            return false;
    }
    public boolean update(int id, String post){
        if ((id >= 0) && (id < Posts.size())){
            Set<String> set = new HashSet<String>(Posts);
            if (!set.contains(post)){
                Posts.set(id, post);
                return true;
            }
        }
        return false;
        
    }
    public boolean delete(int id){
        if ((id >= 0) && (id < Posts.size())){
            if (Posts.get(id)!=null){
                Posts.set(id, null);
                return true;
            }
        }
        return false;
    }
    public List<String> getAll(){
        return Posts;
    }
    public void clear(){
        Posts.clear();
    }
}
