/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBClasses;

import java.util.List;

/**
 *
 * @author justiceforall88
 */
public interface List_Posts {
    int select(String post);
    boolean insert(String post);
    boolean update(int id, String post);
    boolean delete(int id);
    List<String> getAll();
    void clear();
}
