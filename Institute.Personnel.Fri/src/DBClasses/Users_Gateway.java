/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBClasses;

import ObjectClasses.*;
import java.util.*;

/**
 *
 * @author justiceforall88
 */
public class Users_Gateway implements Users{
    
    private static Map Users = new HashMap<String, Object[]>();

    public Object[] select( String id){
        return (Object[]) Users.get(id);
    }
    public boolean insert(String id, Object[] u){
        if (Users.get(id)==null){
            Users.put(id,u);
            return true;
        } 
        else
            return false;
    }
    public boolean update(String id, Object[] u){
        if (Users.get(id)!=null){
            Users.replace(id, u);
            return true;
        }
        else
            return false;
    }
    public boolean delete(String id){
        if (Users.get(id)!=null){
            Users.remove(id);
            return true;
        }
        else
            return false;
    }
    public Map <String, Object[]> getAll(){
        return Users;
    }
    public void clear(){
        Users.clear();
    }
}
