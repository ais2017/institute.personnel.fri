/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBClasses;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author justiceforall88
 */
public class List_ScGrades_Gateway implements List_ScGrades{
    private static List<String> ScGrades = new ArrayList<>(Arrays.asList("к.т.н.", "д.т.н.", "к.ф.-м.н.", "д.ф.-м.н.", "к.х.н.",
        "д.х.н.", "к.э.н.","д.э.н.","к.и.н.","д.и.н.", "к.п.н.", "д.п.н."));
    public int select(String scgrade){
        if (scgrade!=null){
            for (int i=0; i<ScGrades.size(); i++){
                if ((ScGrades.get(i)!=null)&&(ScGrades.get(i).equals(scgrade)))
                    return i;
            }
        }
        return -1;
    }
    public boolean insert(String scgrade){
        Set<String> set = new HashSet<String>(ScGrades);
        if (!set.contains(scgrade)){
            ScGrades.add(scgrade);
            return true;
        }
        else
            return false;
        
    }
    public boolean update(int id, String scgrade){
        if ((id >= 0) && (id < ScGrades.size())){
            Set<String> set = new HashSet<String>(ScGrades);
            if (!set.contains(scgrade)){
                ScGrades.set(id, scgrade);
                return true;
            }
        }
        return false;
        
    }
    public boolean delete(int id){
        if ((id >= 0) && (id < ScGrades.size())){
            if (ScGrades.get(id)!=null){
                ScGrades.set(id, null);
                return true;
            }
        }
        return false;
        
    }
    public List<String> getAll(){
        return ScGrades;
    }
    public void clear(){
        ScGrades.clear();
    }
}
