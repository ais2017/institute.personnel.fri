/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBClasses;

import java.util.List;

/**
 *
 * @author justiceforall88
 */
public interface List_ScRanks {
    int select(String scrank);
    boolean insert(String scrank);
    boolean update(int id, String scrank);
    boolean delete(int id);
    List<String> getAll();
    void clear();
}
