/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBClasses;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author justiceforall88
 */
public class List_ScRanks_Gateway implements List_ScRanks{
    private static List<String> ScRanks = new ArrayList<>(Arrays.asList("Доцент", "Профессор"));
    public int select(String scrank){
        if (scrank!=null){
            for (int i=0; i<ScRanks.size(); i++){
                if ((ScRanks.get(i)!=null)&&(ScRanks.get(i).equals(scrank)))
                    return i;
            }
        }
        return -1;
    }
    public boolean insert(String scrank){
        Set<String> set = new HashSet<String>(ScRanks);
        if (!set.contains(scrank)){
            ScRanks.add(scrank);
            return true;
        }
        else
            return false;
        
    }
    public boolean update(int id, String scrank){
        if ((id >= 0) && (id < ScRanks.size())){
            Set<String> set = new HashSet<String>(ScRanks);
            if (!set.contains(scrank)){
                ScRanks.set(id, scrank);
                return true;
            }
        }
        return false;
    }
    public boolean delete(int id){
        if ((id >= 0) && (id < ScRanks.size())){
            if (ScRanks.get(id)!=null){
                ScRanks.set(id,null);
                return true;
            }
        }
        return false;  
    }
    public List<String> getAll(){
        return ScRanks;
    }
    public void clear(){
        ScRanks.clear();
    }
}
