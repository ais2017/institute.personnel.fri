/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBClasses;

import ObjectClasses.*;
import java.util.Map;

/**
 *
 * @author justiceforall88
 */
public interface Users {
    Object[] select(String id);
    boolean insert(String id, Object[] u);
    boolean update(String id, Object[] u);
    boolean delete(String id);
    Map <String, Object[]> getAll();
    void clear();
}
