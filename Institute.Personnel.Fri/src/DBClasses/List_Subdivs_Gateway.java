/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBClasses;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author justiceforall88
 */
public class List_Subdivs_Gateway implements List_Subdivs{
    private static List<String> Subdivs = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H"));
    public int select(String subdiv){
        if (subdiv!=null){
            for (int i=0; i<Subdivs.size(); i++){
                if ((Subdivs.get(i)!=null)&&(Subdivs.get(i).equals(subdiv)))
                    return i;
            }
        }
        return -1;
    }
    public boolean insert(String subdiv){
        Set<String> set = new HashSet<String>(Subdivs);
        if (!set.contains(subdiv)){
            Subdivs.add(subdiv);
            return true;
        }
        else
            return false;
        
    }
    public boolean update(int id, String subdiv){
        if ((id >= 0) && (id < Subdivs.size())){
            Set<String> set = new HashSet<String>(Subdivs);
            if (!set.contains(subdiv)){
                Subdivs.set(id, subdiv);
                return true;
            }
        }
        return false;
    }
    public boolean delete(int id){
        if ((id >= 0) && (id < Subdivs.size())){
            if (Subdivs.get(id)!=null){
                Subdivs.set(id,null);
                return true;
            }
        }
        return false;  
        
    }
    public List<String> getAll(){
        return Subdivs;
    }
    public void clear(){
        Subdivs.clear();
    }
}
