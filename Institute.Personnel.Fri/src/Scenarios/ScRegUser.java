/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Scenarios;
import ObjectClasses.*;
import ManagingClasses.*;
import DBClasses.*;
import java.io.IOException;
import static junit.framework.Assert.*;

/**
 *
 * @author justiceforall88
 */
public class ScRegUser {
    User u1, u2,u3;
    String l1,l2,l3;
    Object [] obj1,obj2,obj3;
    void init() throws IOException{
        MyFile mf1 = new MyFile("testGetBiomDataArrU1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bmd1 = {bm1};
        l1 = "justiceforall88";
        String p1 = "myqwerty123";
        MyFile mf2 = new MyFile("testGetBiomDataArrU2", "Testing");
        mf2.createFile();
        BiomData bm2 = new BiomData(mf2);
        BiomData []bmd2 = {bm2};
        l2 = "matvey43";
        String p2 = "catcat44";
        MyFile mf3 = new MyFile("testGetBiomDataArrU3", "Testing");
        mf3.createFile();
        BiomData bm3 = new BiomData(mf3);
        BiomData []bmd3 = {bm3};
        l3 = "ZininK96";
        String p3 = "abrakadabra";
        User user1 = new User(l1,p1, bmd1, TypeUser.ADMIN);
        Registration reg1 = new Registration();
        reg1.fillAppl(l1, p1, p1, bmd1, "12345", TypeUser.ADMIN);
        User user2 = new User(l2,p2, bmd2, TypeUser.EMPLOK);
        Registration reg2 = new Registration();
        reg2.fillAppl(l1, p2, p2, bmd2, "54321", TypeUser.EMPLOK);
        User user3 = new User(l3,p3, bmd3, TypeUser.EMPLADM);
        Registration reg3 = new Registration();
        reg2.fillAppl(l3, p3, p3, bmd3, "12321", TypeUser.EMPLADM);
        Object [] o1 = {p1, bmd1, TypeUser.ADMIN};
        Object [] o2 = {p2, bmd2, TypeUser.EMPLOK};
        Object [] o3 = {p3, bmd3, TypeUser.EMPLADM};
        obj1 = o1;
        obj2 = o2;
        obj3 = o3;
        Users p = new Users_Gateway();
        p.insert(l1, obj1);
        p.insert(l2, obj2);
        p.insert(l3, obj3);
    }
    
    public void FalseRegUser_NotCorr() throws IOException{
        Registration reg = new Registration();
        MyFile mf4 = new MyFile("testGetBiomDataArrU2", "Testing");
        mf4.createFile();
        BiomData bm4 = new BiomData(mf4);
        BiomData []bmd4= {bm4};
        boolean b1 = true;
        try{
            reg.fillAppl("VasyaX", "bub", "bub", bmd4, "54436", TypeUser.ADMIN);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error: pas is too small", e.getMessage());
        }
        assertFalse(b1);
        boolean b2 = true;
        try{
            reg.fillAppl("VasyaX", "", "", bmd4, "54436", TypeUser.ADMIN);
        }
        catch(Error e){
            b2 = false;
            assertEquals("Error: pas is required field", e.getMessage());
        }
        assertFalse(b2);
        boolean b3 = true;
        try{
            reg.fillAppl("VasyaX", "vfdvgdrht", "qqqqqqq", bmd4, "54436", TypeUser.ADMIN);
        }
        catch(Error e){
            b3 = false;
            assertEquals("Error: password don't match", e.getMessage());
        }
        assertFalse(b3);
        boolean b4 = true;
        try{
            reg.fillAppl("VaX", "qwerty", "qwerty", bmd4, "54436", TypeUser.ADMIN);
        }
        catch(Error e){
            b4 = false;
            assertEquals("Error: log is too small", e.getMessage());
        }
        assertFalse(b4);
        boolean b5 = true;
        try{
            reg.fillAppl("", "qwerty", "qwerty", bmd4, "54436", TypeUser.ADMIN);
        }
        catch(Error e){
            b5 = false;
            assertEquals("Error: log is required field", e.getMessage());
        }
        assertFalse(b5);
        boolean b6 = true;
        try{
            reg.fillAppl("ArtemZ", "qwerty", "qwerty", bmd4, "1", TypeUser.ADMIN);
        }
        catch(Error e){
            b6 = false;
            assertEquals("Error: invalid idWork", e.getMessage());
        }
        assertFalse(b6);
        boolean b7 = true;
        try{
            reg.fillAppl("ArtemZ", "qwerty", "qwerty", null, "54436", TypeUser.ADMIN);
        }
        catch(Error e){
            b7 = false;
            assertEquals("Error: BiomData array is empty", e.getMessage());
        }
        assertFalse(b7);
    }
    
    public void FalseRegUser_AdmRej() throws IOException{
        Registration reg = new Registration();
        MyFile mf4 = new MyFile("testGetBiomDataArrU2", "Testing");
        mf4.createFile();
        BiomData bm4 = new BiomData(mf4);
        BiomData []bmd4= {bm4};
        reg.fillAppl("VasyaX", "qwerty", "qwerty", bmd4, "54436", TypeUser.ADMIN);
        boolean answ = false;
        boolean b1 = true;
        try{
            reg.introUserToSystem(answ);
        }
        catch(Error e){
            b1 = false;
            assertEquals("No access!", e.getMessage());
        }
        assertFalse(b1);
    }
    public void TrueRegUser() throws IOException{
        Registration reg = new Registration();
        MyFile mf4 = new MyFile("testGetBiomDataArrU2", "Testing");
        mf4.createFile();
        BiomData bm4 = new BiomData(mf4);
        BiomData []bmd4= {bm4};
        reg.fillAppl("VasyaX", "qwerty", "qwerty", bmd4, "54436", TypeUser.ADMIN);
        boolean answ = true;
        boolean b1 = true;
        try{
            reg.introUserToSystem(answ);
        }
        catch(Error e){
            b1 = false;
        }
        assertTrue(b1);
    }
    
    void clear(){
        Users p = new Users_Gateway();
        p.clear();
    }
    public static void main(String[] args) throws IOException {
        ScRegUser sru = new ScRegUser();
        sru.init();
        sru.FalseRegUser_NotCorr();
        sru.FalseRegUser_AdmRej();
        sru.TrueRegUser();
        sru.clear();
    }
}
