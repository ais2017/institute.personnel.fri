/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Scenarios;
import ObjectClasses.*;
import ManagingClasses.*;
import DBClasses.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import static junit.framework.Assert.*;
import org.junit.Assert;

/**
 *
 * @author justiceforall88
 */
public class ScGetInfoMulEmpl {
    Person p1, p2,p3;
    void init() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetContractsPD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetContractsPD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetContractsPD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetContractsPD4", "Testing");
        mf4.createFile();
        MyFile []mf_1 = {mf3};
        MyFile []mf_2 = {mf4};
        String []fio1 = {"Булычев","Михаил","Сергеевич"};
        p1 = new Person(fio1, "Начальник отдела аспирантуры", null , null, "д.т.н.");
        String []fio2 = {"Янов","Александр","Сергеевич"};
        p2 = new Person(fio2, "Преподаватель", "D", null, null, mf_1, null);
        String []fio3 = {"Герасимов","Сергей","Николаевич"};
        p3 = new Person(fio3, "Преподаватель", "E", "Доцент", "к.т.н.", mf_2, c);
        Persons p = new Persons_Gateway();
        p.insert(0, p1);
        p.insert(1, p2);
        p.insert(2, p3);
    }
    public void falseGetInfoM_Empty() throws ParseException{
        Person pd_find2 = new Person(null, "Старший преподаватель");
        RequestData rd2 = new RequestData();
        rd2.setPDParams(pd_find2);
        rd2.queryToDB();
        boolean b2 = true;
        try{
            rd2.orgDataForMulEmpl();
        }
        catch(Error e){
            b2 = false;
            assertEquals("Error: person is not found!", e.getMessage());
        }
        assertFalse(b2);
    }
    
    public void trueGetInfoM() throws ParseException{
        Person pd_find1 = new Person(null,  "Преподаватель");
        RequestData rd1 = new RequestData();
        rd1.setPDParams(pd_find1);
        rd1.queryToDB();
        List <Person> pp = new ArrayList<>();
        boolean b1 = true;
        try{
            pp = rd1.orgDataForMulEmpl();
        }
        catch(Error e){
            b1 = false;
        }
        assertTrue(b1);
        assertEquals(2, pp.size());
        Assert.assertArrayEquals(p2.getFIO(), pp.get(0).getFIO());
        assertEquals(p2.getPost(), pp.get(0).getPost());
        assertEquals(p2.getSubdiv(), pp.get(0).getSubdiv());
        assertEquals(p2.getScRank(), pp.get(0).getScRank());
        assertEquals(p2.getScGrade(), pp.get(0).getScGrade());
        Assert.assertArrayEquals(p2.getFiles(), pp.get(0).getFiles());
        Assert.assertArrayEquals(p2.getContracts(), pp.get(0).getContracts());
        Assert.assertArrayEquals(p3.getFIO(), pp.get(1).getFIO());
        assertEquals(p3.getPost(), pp.get(1).getPost());
        assertEquals(p3.getSubdiv(), pp.get(1).getSubdiv());
        assertEquals(p3.getScRank(), pp.get(1).getScRank());
        assertEquals(p3.getScGrade(), pp.get(1).getScGrade());
        Assert.assertArrayEquals(p3.getFiles(), pp.get(1).getFiles());
        Assert.assertArrayEquals(p3.getContracts(), pp.get(1).getContracts());
    }
    void clear(){
        Persons p = new Persons_Gateway();
        p.clear();
    }
    public static void main(String[] args) throws IOException, ParseException {
        ScGetInfoMulEmpl sgime = new ScGetInfoMulEmpl();
        sgime.init();
        sgime.falseGetInfoM_Empty();
        sgime.trueGetInfoM();
        sgime.clear();
    }
}
