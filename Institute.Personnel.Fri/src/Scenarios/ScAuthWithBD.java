/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Scenarios;
import ObjectClasses.*;
import ManagingClasses.*;
import DBClasses.*;
import java.io.IOException;
import static junit.framework.Assert.*;

/**
 *
 * @author justiceforall88
 */
public class ScAuthWithBD {
    User u1, u2,u3;
    String l1,l2,l3;
    Object [] obj1,obj2,obj3;
    void init() throws IOException{
        MyFile mf1 = new MyFile("testGetBiomDataArrU1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bmd1 = {bm1};
        l1 = "justiceforall88";
        String p1 = "myqwerty123";
        MyFile mf2 = new MyFile("testGetBiomDataArrU2", "Testing");
        mf2.createFile();
        BiomData bm2 = new BiomData(mf2);
        BiomData []bmd2 = {bm2};
        l2 = "matvey43";
        String p2 = "catcat44";
        MyFile mf3 = new MyFile("testGetBiomDataArrU3", "Testing");
        mf3.createFile();
        BiomData bm3 = new BiomData(mf3);
        BiomData []bmd3 = {bm3};
        l3 = "ZininK96";
        String p3 = "abrakadabra";
        User user1 = new User(l1,p1, bmd1, TypeUser.ADMIN);
        Registration reg1 = new Registration();
        reg1.fillAppl(l1, p1, p1, bmd1, "12345", TypeUser.ADMIN);
        User user2 = new User(l2,p2, bmd2, TypeUser.EMPLOK);
        Registration reg2 = new Registration();
        reg2.fillAppl(l2, p2, p2, bmd2, "54321", TypeUser.EMPLOK);
        User user3 = new User(l3,p3, bmd3, TypeUser.EMPLADM);
        Registration reg3 = new Registration();
        reg2.fillAppl(l3, p3, p3, bmd3, "12321", TypeUser.EMPLADM);
        Object [] o1 = {p1, bmd1, TypeUser.ADMIN};
        Object [] o2 = {p2, bmd2, TypeUser.EMPLOK};
        Object [] o3 = {p3, bmd3, TypeUser.EMPLADM};
        obj1 = o1;
        obj2 = o2;
        obj3 = o3;
        Users p = new Users_Gateway();
        p.insert(l1, obj1);
        p.insert(l2, obj2);
        p.insert(l3, obj3);
    }
    public void FalseAuth_Log(){
        MyFile mf2 = new MyFile("testGetBiomDataArrU2", "Testing");
        BiomData bm2 = new BiomData(mf2);
        Authentication auth = new Authentication("Golovkin",bm2);
        assertFalse(auth.findID());
    }
    public void FalseAuth_Bmd() throws IOException{
        MyFile mf2 = new MyFile("testGetBiomDataArrU3", "Testing");
        BiomData bm2 = new BiomData(mf2);
        Authentication auth = new Authentication("justiceforall88",bm2);
        assertTrue(auth.findID());
        assertFalse(auth.isBmdMatch());
    }
    public void TrueAuth() throws IOException{
        MyFile mf2 = new MyFile("testGetBiomDataArrU1", "Testing");
        BiomData bm2 = new BiomData(mf2);
        Authentication auth = new Authentication("justiceforall88",bm2);
        assertTrue(auth.findID());
        assertTrue(auth.isBmdMatch());
    }
    void clear(){
        Users p = new Users_Gateway();
        p.clear();
    }
    public static void main(String[] args) throws IOException {
        ScAuthWithBD scawb = new ScAuthWithBD();
        scawb.init();
        scawb.FalseAuth_Log();
        scawb.FalseAuth_Bmd();
        scawb.TrueAuth();
        scawb.clear();
    }
}
