/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Scenarios;
import ObjectClasses.*;
import ManagingClasses.*;
import DBClasses.*;
import java.io.IOException;
import java.text.ParseException;
import static junit.framework.Assert.*;
import org.junit.Assert;
/**
 *
 * @author justiceforall88
 */
public class ScChangePersonInfo {
    Person p1, p2,p3;
    void init() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetContractsPD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetContractsPD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetContractsPD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetContractsPD4", "Testing");
        mf4.createFile();
        MyFile []mf_1 = {mf3};
        MyFile []mf_2 = {mf4};
        String []fio1 = {"Булычев","Михаил","Сергеевич"};
        p1 = new Person(fio1, "Начальник отдела аспирантуры", null , null, "д.т.н.");
        String []fio2 = {"Янов","Александр","Сергеевич"};
        p2 = new Person(fio2, "Преподаватель", "D", null, null, mf_1, null);
        String []fio3 = {"Герасимов","Сергей","Николаевич"};
        p3 = new Person(fio3, "Преподаватель", "E", "Доцент", "к.т.н.", mf_2, c);
        Persons p = new Persons_Gateway();
        p.insert(0, p1);
        p.insert(1, p2);
        p.insert(2, p3);
    }
    public void FalseChangePI_NotCorr() throws IOException, ParseException{
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLADM);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.EMPLOK);
        User user3 = new User("VadimP", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        MyFile mf1 = new MyFile("testGetContractsPD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetContractsPD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetContractsPD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetContractsPD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        boolean b1 = true;
        try{
            Person pp1 = new Person(fio, "Старший препод", "B", null, "к.т.н.", mf, c);
            cd.makeChanges(user2, pp1, 0);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error: there is no such post", e.getMessage());
        }
        assertFalse(b1);
            
    }
    public void FalseChangePI_NoAccess() throws IOException, ParseException{
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLADM);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.EMPLOK);
        User user3 = new User("VadimP", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        MyFile mf1 = new MyFile("testGetContractsPD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetContractsPD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetContractsPD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetContractsPD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        boolean b1 = true;
        try{
            Person pp1 = new Person(fio, "Старший преподаватель", "B", null, "к.т.н.", mf, c);
            cd.makeChanges(user1, pp1, 0);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
    }
    public void TrueChangePI() throws IOException, ParseException{
                User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLADM);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.EMPLOK);
        User user3 = new User("VadimP", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        MyFile mf1 = new MyFile("testGetContractsPD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetContractsPD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetContractsPD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetContractsPD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        boolean b1 = true;
        Person pp1 = new Person(fio, "Старший преподаватель", "B", null, "к.т.н.", mf, c);
        try{
            
            cd.makeChanges(user2, pp1, 0);
        }
        catch(Error e){
            b1 = false;
        }
        assertTrue(b1);
        Persons p = new Persons_Gateway();
        
        Assert.assertArrayEquals(pp1.getFIO(), p.select(0).getFIO());
        assertEquals(pp1.getPost(), p.select(0).getPost());
        assertEquals(pp1.getSubdiv(), p.select(0).getSubdiv());
        assertEquals(pp1.getScRank(), p.select(0).getScRank());
        assertEquals(pp1.getScGrade(), p.select(0).getScGrade());
        Assert.assertArrayEquals(pp1.getFiles(), p.select(0).getFiles());
        Assert.assertArrayEquals(pp1.getContracts(), p.select(0).getContracts());
    }
    void clear(){
        Persons p = new Persons_Gateway();
        p.clear();
    }
    public static void main(String[] args) throws IOException, ParseException {
        ScChangePersonInfo scpi = new ScChangePersonInfo();
        scpi.init();
        scpi.FalseChangePI_NotCorr();
        scpi.FalseChangePI_NoAccess();
        scpi.TrueChangePI();
        scpi.clear();
    }
}
