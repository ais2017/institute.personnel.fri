/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjectClasses;
import java.io.*;
import java.lang.*;

/**
 *
 * @author azubtsov
 */
public class MyFile {
    private String name;
    private String path;
    
    public MyFile(String n) throws IOException{
        path = "";
        if (n==null||n.equals(""))
            throw new Error("Error: empty name of file");
        else
            name = n;
    }
    public MyFile(String n, String p){
        if (n==null||n.equals(""))
            throw new Error("Error: empty name of file");
        name = n;
        if (p==null)
            path ="";
        else
            path = p;
    }
    
    public String getName(){ return name;}
    public void setName(String n){
        if (n==null||n.equals(""))
            throw new Error("Error: empty name of file");
        name = n;
    }
    
    public String getPath(){ return path; }
    public void setPath(String p){ 
        if (p==null)
            path ="";
        else
            path = p;
    }
    
    public MyFile createFile() throws IOException{
        if ((path == null)||(name == null))
            throw new Error("Error with name of file");
        if ((path.length()== 0) || (path.equals("."))){
            File file = new File(name);
            if (!file.exists()){
                if (!file.createNewFile())
                    throw new Error("Error with creating file");
            }
            return this;
        }
        File dir = new File(path);
        
        if (!dir.exists()){
            if (!dir.mkdir())
                throw new Error("Error with creating dir");
        }
        File file = new File(dir, name);
        if (!file.exists()){
            if (!file.createNewFile())
                throw new Error("Error with creating file");
        }
        return this;
    }
    
    public FileInputStream openFileR() throws FileNotFoundException{

        File file = new File(path, name);
        if (file.exists()){
            FileInputStream fi = new FileInputStream(file);
            return fi;            
        }
        else
            throw new Error("File doesn't exist");
    
    }
    
    public FileOutputStream openFileW() throws FileNotFoundException{

        File file = new File(path, name);
        if (file.exists()){
            FileOutputStream fo = new FileOutputStream(file);
            return fo;
        }
        else
            throw new Error("File doesn't exist");
    
    }
    
    public void changeFile(byte[] t) throws FileNotFoundException, IOException{
        FileOutputStream fo = this.openFileW();
        fo.write(t);
        fo.close();
    }
}
