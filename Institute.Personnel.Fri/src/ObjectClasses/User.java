/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjectClasses;
import java.lang.*;
import java.util.*;
/**
 *
 * @author azubtsov
 */
//enum TypeUser{ADMIN,EMPLADM,EMPLOK, DEFAULT};

public class User {
    private String login;
    private String password;
    private BiomData biomDataArr[] = null;
    private TypeUser type;
    
    public User(String l, String p, TypeUser t){
        if ((l == null) || (l.length()<1))
            throw new Error("Error: login is empty");
        if (l.length()<6)
            throw new Error("Error: login is too small");
        login = l;
        if ((p == null) || (p.length()<1))
            throw new Error("Error: password is empty");
        if (p.length()<6)
            throw new Error("Error: password is too small");
        password = p;
        type = t;
    };
    public User(String l, BiomData[] bmd, TypeUser t){
        if ((l == null) || (l.length()<1))
            throw new Error("Error: login is empty");
        if (l.length()<6)
            throw new Error("Error: login is too small");
        login = l;
        type = t;
        if ((bmd==null)||(bmd.length<1))
            throw new Error("Error: empty bmd array");
        biomDataArr = Arrays.copyOf(bmd, bmd.length);
    }
    public User(String l, String p, BiomData[] bmd, TypeUser t){
        if ((l == null) || (l.length()<1))
            throw new Error("Error: login is empty");
        if (l.length()<6)
            throw new Error("Error: login is too small");
        login = l; 
        if ((p == null) || (p.length()<1))
            throw new Error("Error: password is empty");
        if (p.length()<6)
            throw new Error("Error: password is too small");
        password = p;
        type = t;
        if ((bmd==null)||(bmd.length<1))
            throw new Error("Error: empty bmd array");
        biomDataArr = Arrays.copyOf(bmd, bmd.length);
    }
    
    public String getLogin(){return login;};
    public void setLogin(String log){
        if ((log == null) || (log.length()<1))
            throw new Error("Error: login is empty");
        if (log.length()<6)
            throw new Error("Error: login is too small");
        login = log;
    }
    
    public String getPassword(){return password;}
    public void setPassword(String pas){
        if ((pas == null) || (pas.length()<1))
            throw new Error("Error: password is empty");
        if (pas.length()<6)
            throw new Error("Error: password is too small");
        password = pas;
    }
    
    public BiomData[] getBiomDataArr(){return Arrays.copyOf(biomDataArr, biomDataArr.length);};
    public void setBiomDataArr(BiomData []bm){
        if ((bm != null)&&(bm.length>0))
            biomDataArr = Arrays.copyOf(bm, bm.length);
        else
            throw new Error("Error: empty bmd array");
    };
    
    public TypeUser getType(){return type;};
    public void setType(TypeUser t){type = t;};

}
