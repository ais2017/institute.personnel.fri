/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjectClasses;

import java.io.*;
import java.util.*;

/**
 *
 * @author azubtsov
 */
public class BiomData {
    private MyFile file = null;
    public MyFile getFile(){
        return file;
    }
    public void setFile(MyFile f){
        file = f;
    }
    public BiomData(){
         
    }
    public BiomData(MyFile fl){
         file = fl;
    }
    
    public boolean difference(BiomData bmd) throws FileNotFoundException, IOException{
        FileInputStream fin_this = file.openFileR();
        int i=-1;
        List data_this = new ArrayList <Character>(); 
        while ((i=fin_this.read())!=-1){
            data_this.add((char)i);
        }
        String s1 = data_this.toString();
        
        FileInputStream fin_bmd = bmd.getFile().openFileR();
        int j=-1;
        List data_bmd = new ArrayList <Character>(); 
        while ((j=fin_bmd.read())!=-1){
            data_bmd.add((char)j);
        }
        String s2 = data_bmd.toString();
       
        return (!s1.equals(s2));
    }
    public MyFile create() throws IOException{
        MyFile f = file.createFile();
        return f;
    }
    
}




