/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjectClasses;

import java.util.Arrays;

/**
 *
 * @author azubtsov
 */

public class Application {
    private ApplStat input_status;
    private String input_log;
    private String input_pas;   
    private int input_idWork;
    private TypeUser input_tp;
    private BiomData input_biomDataArr[] = {};
    public Application(){ 
        input_status = ApplStat.JUST_CREAT;
    }
    public Application(String l, String p, int id, TypeUser t, BiomData []bmd){        
        input_status = ApplStat.JUST_CREAT;
        if ((l == null) || (l.length()<1))
            throw new Error("Error: field for login is empty");
        if (l.length()<6)
            throw new Error("Error: login is too small");
        input_log = l;
        if ((p == null) || (p.length()<1))
            throw new Error("Error: field for password is empty");
        if (p.length()<6)
            throw new Error("Error: password is too small");
        input_pas = p;
        if ((id<=9999) || (id>=100000))
            throw new Error("Error: invalid idWork");
        input_idWork = id;
        input_tp = t;
        if ((bmd!=null)&&(bmd.length>0)){
            input_biomDataArr = Arrays.copyOf(bmd, bmd.length);
        }
        else
            throw new Error("BiomData array is empty");
        
    }
    public void setStatus(ApplStat sts){input_status = sts;}
    public ApplStat getStatus(){return input_status;}
    
    public void setLogin(String l){
        if ((l == null) || (l.length()<1))
            throw new Error("Error: field for login is empty");
        if (l.length()<6)
            throw new Error("Error: login is too small");
        input_log = l;
    }
    public String getLogin(){return input_log;}
    
    public void setPassword(String p){
        if ((p == null) || (p.length()<1))
            throw new Error("Error: field for password is empty");
        if (p.length()<6)
            throw new Error("Error: password is too small");
        input_pas = p;
    }
    public String getPassword(){return input_pas;}
    
    public void setIdWork(int id){
        if ((id<=9999) || (id>=100000))
            throw new Error("Error: invalid idWork");
        input_idWork = id;
    }
    public int getIdWork(){return input_idWork;}
    
    public void setTypeUser(TypeUser t){input_tp = t;}
    public TypeUser getTypeUser(){return input_tp;}
    
    public void setBiomDataArr(BiomData []b){
        if ((b!=null)&&(b.length>0))
            input_biomDataArr = Arrays.copyOf(b, b.length);
        else
            throw new Error("Error: BiomData array is empty");
    }
    public BiomData[] getBiomDataArr(){return Arrays.copyOf(input_biomDataArr, input_biomDataArr.length);}
    
    public void fillApplForReg(String l, String p, int id, TypeUser t, BiomData[] bmd){
        if ((l == null) || (l.length()<1))
            throw new Error("Error: field for login is empty");
        if (l.length()<6)
            throw new Error("Error: login is too small");
        input_log = l;
        if ((p == null) || (p.length()<1))
            throw new Error("Error: field for password is empty");
        if (p.length()<6)
            throw new Error("Error: password is too small");
        input_pas = p;
        if ((id<=9999) || (id>=100000))
            throw new Error("Error: invalid idWork");
        input_idWork = id;
        input_tp = t;
        if ((bmd!=null)&&(bmd.length>0)){
            input_biomDataArr = Arrays.copyOf(bmd, bmd.length);
        }
        else
            throw new Error("BiomData array is empty");
    }
}
