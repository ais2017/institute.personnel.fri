/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjectClasses;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.text.*;
import DBClasses.*;
/**
 *
 * @author azubtsov
 */


public class Person {
   
    private String[] FIO = null;
    private String Post = null;
    private String Subdiv = null;
    private String scRank = null;
    private String scGrade = null;
    private MyFile[] files = null;
    private Contract[] contrs = null;
    
    public Person(String[] fio, String pst) throws ParseException{
        Post = pst;
        if ((fio == null) || (fio.length < 1))
            FIO = null;
        else
            FIO = Arrays.copyOf(fio, fio.length);
        checkCorrectData();
    };
    public Person(String[] fio, String pst, String sbdv) throws ParseException{
        Post = pst;
        Subdiv = sbdv;
        if ((fio == null) || (fio.length < 1))
            FIO = null;
        else
            FIO = Arrays.copyOf(fio, fio.length);
        checkCorrectData();
    };
    public Person(String[] fio, String pst, String sbdv, String scrnk) throws ParseException{
        Post = pst;
        Subdiv = sbdv;
        scRank = scrnk;
        if ((fio == null) || (fio.length < 1))
            FIO = null;
        else
            FIO = Arrays.copyOf(fio, fio.length);
        checkCorrectData();
    };
    public Person(String[] fio, String pst, String sbdv, String scrnk, String scgrd) throws ParseException{
        Post = pst;
        Subdiv = sbdv;
        scRank = scrnk;
        scGrade = scgrd;
        if ((fio == null) || (fio.length < 1))
            FIO = null;
        else
            FIO = Arrays.copyOf(fio, fio.length);
        checkCorrectData();        
    };
    public Person(String[] fio, String pst, String sbdv, String scrnk, String scgrd,  
            MyFile[] fs, Contract[] cs) throws ParseException{
        Post = pst;
        Subdiv = sbdv;
        scRank = scrnk;
        scGrade = scgrd;
        if ((fio == null) || (fio.length < 1))
            FIO = null;
        else
            FIO = Arrays.copyOf(fio, fio.length);
        if ((fs == null) || (fs.length < 1))
            files = null;
        else
            files = Arrays.copyOf(fs, fs.length);
        if ((cs == null) || (cs.length < 1))
            contrs = null;
        else
            contrs = Arrays.copyOf(cs, cs.length);
        checkCorrectData();  
    };
    public Contract[] getContracts(){
        return contrs;
    }
    
    public MyFile[] getFiles(){
        return files;
    }
    public void setFiles(MyFile[] mf){
        if (mf != null){
            for (MyFile file : mf) {
                File dir = new File(file.getPath());
                if (!dir.exists()){
                    throw new Error("Dir is not exist");
                }
                File f = new File(dir, file.getName());
                if (!f.exists()){
                    throw new Error("File is not found");                
                }
            }
        }
        if (mf.length < 1)
            files = null;
        else
            files = Arrays.copyOf(mf,mf.length);
        
    }
    public String[] getFIO(){
        return FIO;
    }
    public void setFIO(String []new_fio){
        if (new_fio != null){
            if (new_fio.length < 2 || new_fio.length > 3){
                throw new Error("Incomplete Person Data: Wrong number of FIO String");
            }
            else{
                Pattern p_fio = Pattern.compile("[А-ЯЁ]([а-яё])+");
                Matcher []m_fio = new Matcher[new_fio.length];
                for (int i=0; i<new_fio.length; i++){
                    m_fio[i] = p_fio.matcher(new_fio[i]);
                    if (!m_fio[i].matches()){
                        throw new Error("Incomplete Person Data: Not correctly FIO String");
                    }
                }
            }
        }
        else
            throw new Error("Error: FIO is required parameter");
        FIO = Arrays.copyOf(new_fio, new_fio.length);
            
    }
    
    
    public String getPost(){
        return Post;
    }
    public void setPost(String nPost){
        if (nPost != null){
            List_Posts p = new List_Posts_Gateway();
            if (p.select(nPost) == -1)
                throw new Error("Error: there is no such post");
        }
        else
            throw new Error("Error: Post is required parameter");
        Post = nPost;
    }
    
    public String getSubdiv(){
        return Subdiv;
    }
    public void setSubdiv(String nSubdiv){
        if (nSubdiv != null){
            List_Subdivs lsub = new List_Subdivs_Gateway();
            if (lsub.select(nSubdiv) == -1)
                throw new Error("Error: there is no such subdiv");
        }
        Subdiv = nSubdiv;

    }
    
    public String getScRank(){
        return scRank;
    }
    public void setScRank(String nScRank){
        if (nScRank != null){
            List_ScRanks lrnk = new List_ScRanks_Gateway();
            if (lrnk.select(nScRank) == -1)
                throw new Error("Error: there is no such scrank");
        }
        scRank = nScRank;
    }
    
    public String getScGrade(){
        return scGrade;
    }
    public void setScGrade(String nScGrade){
        if (nScGrade != null){
            List_ScGrades lgrd = new List_ScGrades_Gateway();
            if (lgrd.select(nScGrade) == -1)
                throw new Error("Error: there is no such scgrade");
        }
        scGrade = nScGrade;
    }
    
    public void addFile(MyFile fn){ 
        ArrayList<MyFile> fls;
        if ((files != null)&&(files.length > 0)){
            fls = new ArrayList<MyFile>(Arrays.asList(files));
        }
        else{
            fls = new ArrayList<MyFile>();
        }
        if ((fn.getPath() != null) && (fn.getPath().length()>0) && !(fn.getPath().equals("."))){
            File dir = new File(fn.getPath());
            if (!dir.exists()){
                throw new Error("Dir is not exist");
            }
            File f = new File(dir, fn.getName());
            if (!f.exists()){
                throw new Error("File is not found");                
            }
            fls.add(fn);
            files = new MyFile[fls.size()];
            files = fls.toArray(files);
        }
        else{
            File f = new File(fn.getName());
            if (!f.exists()){
                throw new Error("File is not found");                
            }
            fls.add(fn);
            files = new MyFile[fls.size()];
            files = fls.toArray(files);
        }
        
    }
    public void addContract(Contract c) throws ParseException{
        ArrayList<Contract> cts = new ArrayList<Contract>(Arrays.asList(contrs));
        cts.add(c);
        File fc;
        Pattern p_date = Pattern.compile("[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])");//YYYY-MM-DD
        int cnt_cont = 0;
        for (Contract contr : cts) {
            Matcher m_date = p_date.matcher(contr.getDate());
            if (!m_date.matches()) {
                throw new Error("Incomplete Date of Contract");
            } 
            else {
                SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
                Date new_date = new Date();
                Date contr_date = ft.parse(contr.getDate());
                long diff = new_date.getTime() - contr_date.getTime();
                int days = (int)( diff/(24*60*60*1000));
                if (days < 0){
                    throw new Error("Wrong Date of Contract");
                }
                if (days <= 730 ){
                    cnt_cont++;
                }
                if (cnt_cont>2)
                    throw new Error("Too much of valid Contracts");
                }
                fc = new File(contr.getFile().getPath(), contr.getFile().getName());
                if(!fc.exists()){
                    throw new Error("File is not Found");
                }                
        }
        contrs = new Contract[cts.size()];
        contrs = cts.toArray(contrs);
    }
    public void createPersonFile(String name) throws IOException{  
        MyFile f = new MyFile(name);
        f.createFile();
        addFile(f);
    }
    public void createPersonFile(String name, String path) throws IOException{
        MyFile f = new MyFile(name, path);
        f.createFile();
        addFile(f);
    }
    
    public void checkCorrectData() throws ParseException{
        if (Post != null){
            List_Posts p = new List_Posts_Gateway();
            if (p.select(Post) == -1)
                throw new Error("Error: there is no such post");
        }

        if (Subdiv != null){
            List_Subdivs lsub = new List_Subdivs_Gateway();
            if (lsub.select(Subdiv) == -1)
                throw new Error("Error: there is no such subdiv");
        }
        if (scRank != null){
            List_ScRanks lrnk = new List_ScRanks_Gateway();
            if (lrnk.select(scRank) == -1)
                throw new Error("Error: there is no such scrank");
        }
        if (scGrade != null){
            List_ScGrades lgrd = new List_ScGrades_Gateway();
            if (lgrd.select(scGrade) == -1)
                throw new Error("Error: there is no such scgrade");
        }
        if (FIO != null){
            if (FIO.length < 2 || FIO.length > 3){
                throw new Error("Incomplete Person Data: Wrong number of FIO String");
            }
            else{
                Pattern p_fio = Pattern.compile("[А-ЯЁ]([а-яё])+");
                Matcher []m_fio = new Matcher[FIO.length];
                for (int i=0; i<FIO.length; i++){
                    m_fio[i] = p_fio.matcher(FIO[i]);
                    if (!m_fio[i].matches()){
                        throw new Error("Incomplete Person Data: Not correctly FIO String");
                    }
                }
            }
        }
        if (contrs != null){
            
            File fc;
            Pattern p_date = Pattern.compile("[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])");//YYYY-MM-DD
            int cnt_cont = 0;
            for (Contract contr : contrs) {
                Matcher m_date = p_date.matcher(contr.getDate());
                if (!m_date.matches()) {
                    throw new Error("Incomplete Date of Contract");
                } else {
                    SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
                    Date new_date = new Date();
                    Date contr_date = ft.parse(contr.getDate());
                    long diff = new_date.getTime() - contr_date.getTime();
                    int days = (int)( diff/(24*60*60*1000));
                    if (days < 0){
                        throw new Error("Wrong Date of Contract");
                    }
                    if (days <= 730 ){
                        cnt_cont++;
                    }
                    if (cnt_cont>2)
                        throw new Error("Too much of valid Contracts");
                }
                fc = new File(contr.getFile().getPath(), contr.getFile().getName());
                if(!fc.exists()){
                    throw new Error("File is not Found");
                }                
            }
            
        }
        if (files != null){
            for (MyFile file : files) {
                File dir = new File(file.getPath());
                if (!dir.exists()){
                    throw new Error("Dir is not exist");
                }
                File f = new File(dir, file.getName());
                if (!f.exists()){
                    throw new Error("File is not found");                
                }
            }
        }
    }
}
