/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjectClasses;
import java.util.regex.*;
import java.io.*;
import java.text.*;
import java.util.*;
/**
 *
 * @author azubtsov
 */
public class Contract {
    private String date;
    private MyFile file;
    
    public Contract(String dt, MyFile fl) throws ParseException{
        File fc;
        Pattern p_date = Pattern.compile("[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])");//YYYY-MM-DD
        Matcher m_date = p_date.matcher(dt);
        if (!m_date.matches()) {
            throw new Error("Incomplete Date of Contract");
        } 
        else {
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
            Date new_date = new Date();
            Date contr_date = ft.parse(dt);
            long diff = new_date.getTime() - contr_date.getTime();
            int days = (int)( diff/(24*60*60*1000));
            if (days < 0){
                throw new Error("Wrong Date of Contract");
            }
        }
        fc = new File(fl.getPath(), fl.getName());
        if(!fc.exists()){
            throw new Error("File is not Found");
        }
        date = dt;
        file = fl;
    }

    public String getDate(){return date;}
    public void setDate(String dt) throws ParseException{
        Pattern p_date = Pattern.compile("[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[0-1])");//YYYY-MM-DD
        Matcher m_date = p_date.matcher(dt);
        if (!m_date.matches()) {
            throw new Error("Incomplete Date of Contract");
        } 
        else {
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
            Date new_date = new Date();
            Date contr_date = ft.parse(dt);
            long diff = new_date.getTime() - contr_date.getTime();
            int days = (int)( diff/(24*60*60*1000));
            if (days < 0){
                throw new Error("Wrong Date of Contract");
            }
        }
        date = dt;
    }
    
    public MyFile getFile(){return file;}    
    public void setFile(MyFile fl){
        File fc;
        fc = new File(fl.getPath(), fl.getName());
        if(!fc.exists()){
            throw new Error("File is not Found");
        }
        file = fl;
    }

}
