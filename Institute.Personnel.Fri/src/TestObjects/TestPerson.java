/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestObjects;
import ObjectClasses.*;
import java.io.*;
import java.text.ParseException;
import static junit.framework.Assert.*;

/**
 *
 * @author justiceforall88
 */
public class TestPerson {
    
    
    public void testGetFIO() throws ParseException{
        String []exp = {"Чевычелов","Матвей","Иванович"};
        Person p1 = new Person(exp, "Ректор");
        String []act = p1.getFIO();
        assertEquals(3, act.length);
        assertEquals("Чевычелов", act[0]);
        assertEquals("Матвей", act[1]);
        assertEquals("Иванович", act[2]);
    }
    public void testSetFIO() throws ParseException{
        String []start = {"Чевычелов","Матвей","Иванович"};
        String []exp = {"Зинин","Константин","Владимирович"};
        Person p1 = new Person(start, "Ректор");
        p1.setFIO(exp);
        String []act = p1.getFIO();
        assertEquals(3, act.length);
        assertEquals("Зинин", act[0]);
        assertEquals("Константин", act[1]);
        assertEquals("Владимирович", act[2]);
        
    }
    public void testGetPost() throws ParseException{
        String []fio = {"Пупкин","Василий","Иванович"};
        Person p1 = new Person(fio, "Заведующий кафедрой", "D", "Профессор", "д.ф.-м.н.");
        assertEquals("Заведующий кафедрой", p1.getPost());
    }
    public void testSetPost() throws ParseException{
        String []fio = {"Пупкин","Василий","Иванович"};
        Person p1 = new Person(fio, "Заведующий кафедрой", "D", "Профессор", "д.ф.-м.н.");
        p1.setPost("Заместитель декана факультета");
        assertEquals("Заместитель декана факультета", p1.getPost());
    }
    public void testGetSubdiv() throws ParseException{
        String []fio = {"Пупкин","Василий","Иванович"};
        Person p1 = new Person(fio, "Заведующий кафедрой", "D", "Профессор", "д.ф.-м.н.");
        assertEquals("D", p1.getSubdiv());
    }
    public void testSetSubdiv() throws ParseException{
        String []fio = {"Пупкин","Василий","Иванович"};
        Person p1 = new Person(fio, "Заведующий кафедрой", "D", "Профессор", "д.ф.-м.н.");
        p1.setSubdiv("B");
        assertEquals("B", p1.getSubdiv());
    }
    public void testGetScRank() throws ParseException{
        String []fio = {"Пупкин","Василий","Иванович"};
        Person p1 = new Person(fio, "Заведующий кафедрой", "D", "Доцент", "д.ф.-м.н.");
        assertEquals("Доцент", p1.getScRank());
    }
    public void testSetScRank() throws ParseException{
        String []fio = {"Пупкин","Василий","Иванович"};
        Person p1 = new Person(fio, "Заведующий кафедрой", "D", "Доцент", "д.ф.-м.н.");
        p1.setScRank("Профессор");
        assertEquals("Профессор", p1.getScRank());
    }
    public void testGetScGrade() throws ParseException{
        String []fio = {"Пупкин","Василий","Иванович"};
        Person p1 = new Person(fio, "Заведующий кафедрой", "D", "Профессор", "к.ф.-м.н.");
        assertEquals("к.ф.-м.н.", p1.getScGrade());
    }
    public void testSetScGrade() throws ParseException{
        String []fio = {"Пупкин","Василий","Иванович"};
        Person p1 = new Person(fio, "Заведующий кафедрой", "D", "Профессор", "к.ф.-м.н.");
        p1.setScGrade("д.ф.-м.н.");
        assertEquals("д.ф.-м.н.", p1.getScGrade());
    }
    public void testGetContracts() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetContractsPD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetContractsPD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c_expect = {c1,c2};
        MyFile mf3 = new MyFile("testGetContractsPD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetContractsPD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        Person p1 = new Person(fio, "Старший преподаватель", "B", null, "к.т.н.", mf, c_expect);
        Contract []c_actual = p1.getContracts();
        assertEquals(2, c_actual.length);
        assertEquals(c1, c_actual[0]);
        assertEquals(c2, c_actual[1]);
    }
   
    public void testGetFiles() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetFilesPD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetFilesPD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetFilesPD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetFilesPD4", "Testing");
        mf4.createFile();
        MyFile []mf_expect = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        Person p1 = new Person(fio, "Старший преподаватель", "B", null, "к.т.н.", mf_expect, c);
        MyFile []mf_actual = p1.getFiles();
        assertEquals(2, mf_actual.length);
        assertEquals(mf3, mf_actual[0]);
        assertEquals(mf4, mf_actual[1]);
    }
    public void testSetFiles() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testSetFilesPD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-04-09", mf1);
        MyFile mf2 = new MyFile("testSetFilesPD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-07-16", mf2);
        Contract []c= {c1,c2};
        MyFile mf3 = new MyFile("testSetFilesPD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testSetFilesPD4", "Testing");
        mf4.createFile();
        MyFile []mf_start = {mf3,mf4};
        MyFile mf5 = new MyFile("testSetFilesPD5", "Testing");
        mf5.createFile();
        MyFile mf6 = new MyFile("testSetFilesPD6", "Testing");
        mf6.createFile();
        MyFile []mf_expect = {mf5,mf6};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        Person p1 = new Person(fio, "Старший преподаватель", "B", null, "к.т.н.", mf_start, c);
        p1.setFiles(mf_expect);
        MyFile []mf_actual = p1.getFiles();
        assertEquals(2, mf_actual.length);
        assertEquals(mf5, mf_actual[0]);
        assertEquals(mf6, mf_actual[1]);
    }
    public void testAddFile() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testAddFilePD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-04-09", mf1);
        MyFile mf2 = new MyFile("testAddFilePD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-07-16", mf2);
        Contract []c= {c1,c2};
        MyFile mf3 = new MyFile("testAddFilePD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testAddFilePD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        MyFile mf5 = new MyFile("testAddFilePD5", "Testing");
        mf5.createFile();
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        Person p1 = new Person(fio, "Старший преподаватель", "B", null, "к.т.н.", mf, c);
        p1.addFile(mf5);
        MyFile []mf_actual = p1.getFiles();
        assertEquals(3, mf_actual.length);
        assertEquals(mf3, mf_actual[0]);
        assertEquals(mf4, mf_actual[1]);
        assertEquals(mf5, mf_actual[2]);
    }
    public void testAddContract() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testAddFilePD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-04-09", mf1);
        MyFile mf2 = new MyFile("testAddFilePD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-07-16", mf2);
        Contract []c= {c1};
        MyFile mf3 = new MyFile("testAddFilePD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testAddFilePD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        Person p1 = new Person(fio, "Старший преподаватель", "B", null, "к.т.н.", mf, c);
        p1.addContract(c2);
        Contract []c_actual = p1.getContracts();
        assertEquals(2, c_actual.length);
        assertEquals(c1, c_actual[0]);
        assertEquals(c2, c_actual[1]);
    }
    public void testCreatePersonFile() throws IOException, ParseException{
        String []fio = {"Соловьев","Глеб","Алексеевич"};
        Person p1 = new Person(fio, "Ассистент", "A");
        p1.createPersonFile("testCreatePersonPD1");
        File name = new File("testCreatePersonPD1");
        assertTrue(name.exists());
        assertEquals(1, p1.getFiles().length);
    }
    public void testCreatePersonFileWithPath() throws IOException, ParseException{
        String []fio = {"Соловьев","Глеб","Алексеевич"};
        Person p1 = new Person(fio, "Ассистент", "A");
        p1.createPersonFile("testCreatePersonWPPD1", "Testing");
        File dir = new File("Testing");
        assertTrue(dir.exists());
        File file = new File(dir, "testCreatePersonWPPD1");
        assertTrue(file.exists());
        assertEquals(1, p1.getFiles().length);
    }
    public void testCheckCorrectData() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetContractsPD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetContractsPD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetContractsPD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetContractsPD4", "Testing");
        mf4.createFile();
        MyFile []mf_1 = {mf3};
        MyFile []mf_2 = {mf4};
        String []fio1 = {"Булычев","Михаил","Сергеевич"};
        Person p1 = new Person(fio1, "Начальник отдела аспирантуры", null , null, "к.т.н.");
        String []fio2 = {"Янов","Александр","Сергеевич"};
        Person p2 = new Person(fio2, "Преподаватель", "D", null, null, mf_1, null);
        String []fio3 = {"Герасимов","Сергей","Николаевич"};
        Person p3 = new Person(fio3, "Доцент", "E", "Доцент", "д.х.н.", mf_2, c);
        boolean b1 = true;
        try{
            p1.checkCorrectData();
            p2.checkCorrectData();
            p3.checkCorrectData();
        }
        catch(Error e){
            b1 = false;
        }
        assertTrue(b1);
        boolean b2 = true;
        try{
            String []fio4 = {"Абакумов"};
            Person p4 = new Person(fio4, "Преподаватель", "C");
            p4.checkCorrectData();
        }
        catch(Error e){
            b2 = false;
            assertEquals("Incomplete Person Data: Wrong number of FIO String", e.getMessage());
        }
        assertFalse(b2);
        boolean b3 = true;
        try{
            String []fio5 = {"Абакумов", "Максим", "Сергеевич"};
            Person p5 = new Person(fio5, "Прораб", "C");
            p5.checkCorrectData();
        }
        catch(Error e){
            b3 = false;
            assertEquals("Error: there is no such post", e.getMessage());
        }
        assertFalse(b3);
        boolean b4 = true;
        try{
            String []fio6 = {"Абакумов", "Максим", "Сергеевич"};
            Person p6 = new Person(fio6, "Преподаватель", "W");
            p6.checkCorrectData();
        }
        catch(Error e){
            b4 = false;
            assertEquals("Error: there is no such subdiv", e.getMessage());
        }
        assertFalse(b4);
        boolean b5 = true;
        try{
            String []fio7 = {"Матрынов", "Евгений", "Александович"};
            Person p7 = new Person(fio7, "Заведующий кафедрой", "D", "Магистр", "к.э.н.");
            p7.checkCorrectData();
        }
        catch(Error e){
            b5 = false;
            assertEquals("Error: there is no such scrank", e.getMessage());
        }
        assertFalse(b5);
        boolean b6 = true;
        try{
            String []fio8 = {"Матрынов", "Евгений", "Александович"};
            Person p8 = new Person(fio8, "Заведующий кафедрой", "D", "Профессор", "к.н.");
            p8.checkCorrectData();
        }
        catch(Error e){
            b6 = false;
            assertEquals("Error: there is no such scgrade", e.getMessage());
        }
        assertFalse(b6);
        boolean b7 = true;
        try{
            MyFile mf91 = new MyFile("testChechCorrectDataPD91", "Testing");
            mf91.createFile();
            Contract c91 = new Contract("2017-06-25", mf91);
            MyFile mf92 = new MyFile("testChechCorrectDataPD92", "Testing");
            mf92.createFile();
            Contract c92 = new Contract("2017-08-31", mf92);
            MyFile mf93 = new MyFile("testChechCorrectDataPD93", "Testing");
            mf93.createFile();
            Contract c93 = new Contract("2017-10-07", mf93);
            Contract []c9 = {c91,c92,c93};
            String []fio9 = {"Спиридонов", "Вадим", "Юрьевич"};
            Person p9 = new Person(fio9, "Заведующий кафедрой", "D", "Профессор", "к.т.н.",null, c9);
            p9.checkCorrectData();
        }
        catch(Error e){
            b7 = false;
            assertEquals("Too much of valid Contracts", e.getMessage());
        }
        assertFalse(b7);
        boolean b8 = true;
        try{
            MyFile mf101 = new MyFile("testChechCorrectDataPD101", "Testing");
            mf101.createFile();
            Contract c101 = new Contract("2017-05-32", mf101);
            MyFile mf102 = new MyFile("testChechCorrectDataPD102", "Testing");
            mf102.createFile();
            Contract c102 = new Contract("2017-08-31", mf102);
            Contract []c10 = {c101,c102};
            String []fio10 = {"Спиридонов", "Вадим", "Юрьевич"};
            Person p10 = new Person(fio10, "Заведующий кафедрой", "D", "Профессор", "к.т.н.",null, c10);
            p10.checkCorrectData();
        }
        catch(Error e){
            b8 = false;
            assertEquals("Incomplete Date of Contract", e.getMessage());
        }
        assertFalse(b8);
        boolean b9 = true;
        try{
            MyFile mf111 = new MyFile("testChechCorrectDataPD111", "Testing");
            mf111.createFile();
            Contract c111 = new Contract("2093-05-31", mf111);
            MyFile mf112 = new MyFile("testChechCorrectDataPD112", "Testing");
            mf112.createFile();
            Contract c112 = new Contract("2017-08-31", mf112);
            Contract []c11 = {c111,c112};
            String []fio11 = {"Спиридонов", "Вадим", "Юрьевич"};
            Person p11 = new Person(fio11, "Заведующий кафедрой", "D", "Профессор", "к.т.н.",null, c11);
            p11.checkCorrectData();
        }
        catch(Error e){
            b9 = false;
            assertEquals("Wrong Date of Contract", e.getMessage());
        }
        assertFalse(b9);
        
    }
    public static void main(String[] args) throws IOException, ParseException {
        
        TestPerson tpd = new TestPerson();
               
        tpd.testGetFIO();
        tpd.testSetFIO();
        tpd.testGetPost();
        tpd.testSetPost();
        tpd.testGetSubdiv();
        tpd.testSetSubdiv();
        tpd.testGetScRank();
        tpd.testSetScRank();
        tpd.testGetScGrade();
        tpd.testSetScGrade();
        tpd.testGetFiles();
        tpd.testSetFiles();
        tpd.testGetContracts();
        tpd.testAddFile();
        tpd.testAddContract();
        tpd.testCreatePersonFile();
        tpd.testCreatePersonFileWithPath();
        tpd.testCheckCorrectData();
    }
}

