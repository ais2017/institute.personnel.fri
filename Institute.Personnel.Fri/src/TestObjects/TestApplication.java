/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestObjects;
import ObjectClasses.*;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static junit.framework.Assert.*;
//import junit.framework.TestCase;
//import junit.framework.Assert;
/**
 *
 * @author justiceforall88
 */
public class TestApplication {
    
    public void testApplicationInit() throws IOException{
        MyFile mf1 = new MyFile("testApplicationInitA1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bm = {bm1};
        boolean b1 = true;
        try{
            Application appl1 = new Application( null, "ctghuuj", 12955, TypeUser.ADMIN, bm);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error: field for login is empty", e.getMessage());
        }
        assertFalse(b1);
        boolean b2 = true;
        try{
            Application appl2 = new Application( "", "ctghuuj", 12955, TypeUser.ADMIN, bm);
        }
        catch(Error e){
            b2 = false;
            assertEquals("Error: field for login is empty", e.getMessage());
        }
        assertFalse(b2);
        boolean b3 = true;
        try{
            Application appl3 = new Application("myl", "ctghuuj" , 12955, TypeUser.ADMIN, bm);
        }
        catch(Error e){
            b3 = false;
            assertEquals("Error: login is too small", e.getMessage());
        }
        assertFalse(b3);
        boolean b4 = true;
        try{
            Application appl4 = new Application("mylogin", null, 12955, TypeUser.ADMIN, bm);
        }
        catch(Error e){
            b4 = false;
            assertEquals("Error: field for password is empty", e.getMessage());
        }
        assertFalse(b4);
        boolean b5 = true;
        try{
            Application appl5 = new Application("mylogin", "", 12955, TypeUser.ADMIN, bm);
        }
        catch(Error e){
            b5 = false;
            assertEquals("Error: field for password is empty", e.getMessage());
        }
        assertFalse(b5);
        boolean b6 = true;
        try{
            Application appl6 = new Application("mylogin", "buba", 12955, TypeUser.ADMIN, bm);
        }
        catch(Error e){
            b6 = false;
            assertEquals("Error: password is too small", e.getMessage());
        }
        assertFalse(b6);
        for (int i=0; i<9999; i++){
            boolean b7 = true;
            try{
                Application appl7 = new Application("mylogin", "bubaQS", i, TypeUser.ADMIN, bm);
            }
            catch(Error e){
                b7 = false;
                assertEquals("Error: invalid idWork", e.getMessage());
            }
            assertFalse(b7);
        }
        for (int i=10000; i<99999; i++){
            boolean b7 = true;
            try{
                Application appl7 = new Application("mylogin", "bubaQS", i, TypeUser.ADMIN, bm);
            }
            catch(Error e){
                b7 = false;
                assertEquals("Error: invalid idWork", e.getMessage());
            }
            assertTrue(b7);
        }
        boolean b8 = true;
        try{
            Application appl8 = new Application("mylogin", "bubaQS", 22745, TypeUser.ADMIN, null);
        }
        catch(Error e){
            b8 = false;
            assertEquals("BiomData array is empty", e.getMessage());
        }
        assertFalse(b8);
        BiomData []new_bm = {};
        boolean b9 = true;
        try{
            Application appl9 = new Application("mylogin", "bubaQS", 22745, TypeUser.ADMIN, new_bm);
        }
        catch(Error e){
            b9 = false;
            assertEquals("BiomData array is empty", e.getMessage());
        }
        assertFalse(b9);
    }
    
    public void testGetStatus(){
        Application appl1 = new Application();
        assertEquals(ApplStat.JUST_CREAT,appl1.getStatus());
    }
    
    public void testSetStatus(){
        Application appl1 = new Application();
        appl1.setStatus(ApplStat.FILL_BUT_NOT_CHECK);
        assertEquals(ApplStat.FILL_BUT_NOT_CHECK, appl1.getStatus());
    }
    
    public void testGetLog() throws IOException{
        MyFile mf1 = new MyFile("testGetLogA1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bm = {bm1};
        Application appl1 = new Application("mylogin", "lklklk", 16352, TypeUser.ADMIN, bm);
        assertEquals("mylogin", appl1.getLogin());
    }
    
    public void testSetLog() throws IOException{
        MyFile mf1 = new MyFile("testSetLogA1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bm = {bm1};
        Application appl1 = new Application("mylogin", "lklklk", 16352, TypeUser.ADMIN, bm);
        appl1.setLogin("changelog");
        assertEquals("changelog", appl1.getLogin());
        boolean b1 = true;
        try{
            appl1.setLogin(null);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error: field for login is empty", e.getMessage());
        }
        assertFalse(b1);
        boolean b2 = true;
        try{
            appl1.setLogin("");
        }
        catch(Error e){
            b2 = false;
            assertEquals("Error: field for login is empty", e.getMessage());
        }
        assertFalse(b2);
        boolean b3 = true;
        try{
            appl1.setLogin("myl");
        }
        catch(Error e){
            b3 = false;
            assertEquals("Error: login is too small", e.getMessage());
        }
        assertFalse(b3);
    }
    
    public void testGetPas() throws IOException{
        MyFile mf1 = new MyFile("testGetPasA1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bm = {bm1};
        Application appl1 = new Application("werfif", "pass123", 16352, TypeUser.ADMIN, bm);
        assertEquals("pass123", appl1.getPassword());
    }
    
    public void testSetPas() throws IOException{
        MyFile mf1 = new MyFile("testSetPasA1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bm = {bm1};
        Application appl1 = new Application("werfif", "pass123", 16352, TypeUser.ADMIN, bm);
        appl1.setPassword("qwerty");
        assertEquals("qwerty", appl1.getPassword());
        boolean b1 = true;
        try{
            appl1.setPassword(null);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error: field for password is empty", e.getMessage());
        }
        assertFalse(b1);
        boolean b2 = true;
        try{
            appl1.setPassword("");
        }
        catch(Error e){
            b2 = false;
            assertEquals("Error: field for password is empty", e.getMessage());
        }
        assertFalse(b2);
        boolean b3 = true;
        try{
            appl1.setPassword("abc");
        }
        catch(Error e){
            b3 = false;
            assertEquals("Error: password is too small", e.getMessage());
        }
        assertFalse(b3);
    }
    
    public void testGetIdWork() throws IOException{
        MyFile mf1 = new MyFile("testGetIdWorkA1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bm = {bm1};
        Application appl1 = new Application( "asdcer", "ctghuuj", 12955, TypeUser.ADMIN, bm);
        assertEquals(12955, appl1.getIdWork());
    }
    
    public void testSetIdWork() throws IOException{
        MyFile mf1 = new MyFile("testSetIdWorkA1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bm = {bm1};
        Application appl1 = new Application("asdcer", "ctghuuj", 12955, TypeUser.ADMIN, bm);
        for (int i=0; i<9999; i++){
            boolean b7 = true;
            try{
                appl1.setIdWork(i);
            }
            catch(Error e){
                b7 = false;
                assertEquals("Error: invalid idWork", e.getMessage());
            }
            assertFalse(b7);
        }
        for (int i=10000; i<99999; i++){
            boolean b7 = true;
            try{
               appl1.setIdWork(i);
            }
            catch(Error e){
                b7 = false;
            }
            assertTrue(b7);
        }
    }
    
    public void testGetTypeUser() throws IOException{
        MyFile mf1 = new MyFile("testGetTypeUserA1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bm = {bm1};
        Application appl1 = new Application("nppknkk", "dfghjkl", 59358, TypeUser.ADMIN, bm);
        assertEquals(TypeUser.ADMIN, appl1.getTypeUser());
    }
    
    public void testSetTypeUser() throws IOException{
        MyFile mf1 = new MyFile("testSetTypeUserA1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bm = {bm1};
        Application appl1 = new Application("nppknof", "dfghjkl", 59358, TypeUser.ADMIN, bm);
        appl1.setTypeUser(TypeUser.EMPLOK);
        assertEquals(TypeUser.EMPLOK, appl1.getTypeUser());
    }
    
    public void testGetBiomData() throws IOException{
        MyFile mf1 = new MyFile("testGetBiomDataA1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        MyFile mf2 = new MyFile("testGetBiomDataA2", "Testing");
        mf2.createFile();
        BiomData bm2 = new BiomData(mf2);
        MyFile mf3 = new MyFile("testGetBiomDataA3", "Testing");
        mf3.createFile();
        BiomData bm3 = new BiomData(mf3);
        BiomData []bm = {bm1, bm2, bm3};
        Application appl1 = new Application( "werfqferf", "csdcccw", 88237, TypeUser.ADMIN, bm);
        assertEquals(bm.length, appl1.getBiomDataArr().length);
        for (int i=0; i<bm.length; i++){
            assertEquals(bm[i], appl1.getBiomDataArr()[i]);
        }
    }
    
    public void testSetBiomData() throws IOException{
        MyFile mf1 = new MyFile("testGetBiomDataA1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        MyFile mf2 = new MyFile("testGetBiomDataA2", "Testing");
        mf2.createFile();
        BiomData bm2 = new BiomData(mf2);
        MyFile mf3 = new MyFile("testGetBiomDataA3", "Testing");
        mf3.createFile();
        BiomData bm3 = new BiomData(mf3);
        BiomData []bm = {bm1, bm2, bm3};
        MyFile mf4 = new MyFile("testGetBiomDataA4", "Testing");
        mf4.createFile();
        BiomData bm4 = new BiomData(mf4);
        MyFile mf5 = new MyFile("testGetBiomDataA5", "Testing");
        mf5.createFile();
        BiomData bm5 = new BiomData(mf5);
        BiomData []bm_new = {bm4, bm5};
        Application appl1 = new Application("werfqferf", "csdcccw", 88237, TypeUser.ADMIN, bm);
        appl1.setBiomDataArr(bm_new);
        assertEquals(bm_new.length, appl1.getBiomDataArr().length);
        for (int i=0; i<bm_new.length; i++){
            assertEquals(bm_new[i], appl1.getBiomDataArr()[i]);
        }
        boolean b8 = true;
        try{
            appl1.setBiomDataArr(null);
        }
        catch(Error e){
            b8 = false;
            assertEquals("Error: BiomData array is empty", e.getMessage());
        }
        assertFalse(b8);
        BiomData []bm_newnew = {};
        boolean b9 = true;
        try{
            appl1.setBiomDataArr(bm_newnew);
        }
        catch(Error e){
            b9 = false;
            assertEquals("Error: BiomData array is empty", e.getMessage());
        }
        assertFalse(b9);
    }
    
    public void testFillAplForReg() throws IOException{
        MyFile mf1 = new MyFile("testFillApplForRegA1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        MyFile mf2 = new MyFile("testFillApplForRegA2", "Testing");
        mf2.createFile();
        BiomData bm2 = new BiomData(mf2);
        BiomData []bm = {bm1, bm2};
        Application appl0 = new Application();
        appl0.fillApplForReg("lollol", "bolbol", 15412, TypeUser.ADMIN, bm);
        assertEquals("lollol", appl0.getLogin());
        assertEquals("bolbol", appl0.getPassword());
        assertEquals(15412, appl0.getIdWork());
        assertEquals(TypeUser.ADMIN, appl0.getTypeUser());
        assertEquals(bm.length, appl0.getBiomDataArr().length);
        for (int i=0; i<bm.length; i++){
            assertEquals(bm[i], appl0.getBiomDataArr()[i]);
        }
        boolean b1 = true;
        try{
            Application appl1 = new Application();
            appl1.fillApplForReg(null, "ctghuuj", 12955, TypeUser.ADMIN, bm);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error: field for login is empty", e.getMessage());
        }
        assertFalse(b1);
        boolean b2 = true;
        try{
            Application appl2 = new Application();
            appl2.fillApplForReg( "", "ctghuuj", 12955, TypeUser.ADMIN, bm);
        }
        catch(Error e){
            b2 = false;
            assertEquals("Error: field for login is empty", e.getMessage());
        }
        assertFalse(b2);
        boolean b3 = true;
        try{
            Application appl3 = new Application();
            appl3.fillApplForReg("myl", "ctghuuj" , 12955, TypeUser.ADMIN, bm);
        }
        catch(Error e){
            b3 = false;
            assertEquals("Error: login is too small", e.getMessage());
        }
        assertFalse(b3);
        boolean b4 = true;
        try{
            Application appl4 = new Application();
            appl4.fillApplForReg("mylogin", null, 12955, TypeUser.ADMIN, bm);
        }
        catch(Error e){
            b4 = false;
            assertEquals("Error: field for password is empty", e.getMessage());
        }
        assertFalse(b4);
        boolean b5 = true;
        try{
            Application appl5 = new Application();
            appl5.fillApplForReg("mylogin", "", 12955, TypeUser.ADMIN, bm);
        }
        catch(Error e){
            b5 = false;
            assertEquals("Error: field for password is empty", e.getMessage());
        }
        assertFalse(b5);
        boolean b6 = true;
        try{
            Application appl6 = new Application();
            appl6.fillApplForReg("mylogin", "buba", 12955, TypeUser.ADMIN, bm);
        }
        catch(Error e){
            b6 = false;
            assertEquals("Error: password is too small", e.getMessage());
        }
        assertFalse(b6);
        for (int i=0; i<9999; i++){
            boolean b7 = true;
            try{
                Application appl7 = new Application();
                appl7.fillApplForReg("mylogin", "bubaQS", i, TypeUser.ADMIN, bm);
            }
            catch(Error e){
                b7 = false;
                assertEquals("Error: invalid idWork", e.getMessage());
            }
            assertFalse(b7);
        }
        for (int i=10000; i<99999; i++){
            boolean b7 = true;
            try{
                Application appl7 = new Application();
                appl7.fillApplForReg("mylogin", "bubaQS", i, TypeUser.ADMIN, bm);
            }
            catch(Error e){
                b7 = false;
                assertEquals("Error: invalid idWork", e.getMessage());
            }
            assertTrue(b7);
        }
        boolean b8 = true;
        try{
            Application appl8 = new Application();
            appl8.fillApplForReg("mylogin", "bubaQS", 22745, TypeUser.ADMIN, null);
        }
        catch(Error e){
            b8 = false;
            assertEquals("BiomData array is empty", e.getMessage());
        }
        assertFalse(b8);
        BiomData []new_bm = {};
        boolean b9 = true;
        try{
            Application appl9 = new Application();
            appl9.fillApplForReg("mylogin", "bubaQS", 22745, TypeUser.ADMIN, new_bm);
        }
        catch(Error e){
            b9 = false;
            assertEquals("BiomData array is empty", e.getMessage());
        }
        assertFalse(b9);
    }
    
    public static void main(String[] args) throws IOException{
        
        TestApplication ta = new TestApplication();
        ta.testApplicationInit();
        ta.testGetStatus();
        ta.testSetStatus();
        ta.testGetLog();
        ta.testSetLog();
        ta.testGetPas();
        ta.testSetPas();
        ta.testGetIdWork();
        ta.testSetIdWork();
        ta.testGetTypeUser();
        ta.testSetTypeUser();
        ta.testGetBiomData();
        ta.testSetBiomData();
        ta.testFillAplForReg();
    }
}
