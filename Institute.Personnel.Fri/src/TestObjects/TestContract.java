/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestObjects;
import ObjectClasses.*;
import java.io.*;
import java.text.ParseException;
import java.util.*;
import static junit.framework.Assert.*;

/**
 *
 * @author justiceforall88
 */
public class TestContract {
    
    public void testContractInit() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testContractInitC1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-04-10", mf1);
        MyFile mf2 = new MyFile("testContractInitC2", "Testing");
        assertEquals("2017-04-10", c1.getDate());
        boolean b2 = true;
        try{
            Contract c2 = new Contract("2017-04-10", mf2);
        }
        catch(Error e){
            b2 = false;
            assertEquals("File is not Found", e.getMessage());
        }
        assertFalse(b2);
        boolean b3 = true;
        try{
            Contract c3 = new Contract("2019-04-10", mf1);
        }
        catch(Error e){
            b3 = false;
            assertEquals("Wrong Date of Contract", e.getMessage());
        }
        assertFalse(b3);
        boolean b4 = true;
        try{
            Contract c4 = new Contract("bldsfs", mf1);
        }
        catch(Error e){
            b4 = false;
            assertEquals("Incomplete Date of Contract", e.getMessage());
        }
        assertFalse(b4);
        
        
    }
    public void testGetDate() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetDateC1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-04-10", mf1);
        assertEquals("2017-04-10", c1.getDate());
    }
    public void testSetDate() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testSetDateC1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-04-10", mf1);
        MyFile mf2 = new MyFile("testSetDateC2", "Testing");
        mf2.createFile();
        c1.setDate("2017-10-21");
        assertEquals("2017-10-21", c1.getDate());
        boolean b3 = true;
        try{
            c1.setDate("2019-04-10");
        }
        catch(Error e){
            b3 = false;
            assertEquals("Wrong Date of Contract", e.getMessage());
        }
        assertFalse(b3);
        boolean b4 = true;
        try{
            c1.setDate("bldsfs");
        }
        catch(Error e){
            b4 = false;
            assertEquals("Incomplete Date of Contract", e.getMessage());
        }
        assertFalse(b4);
    }
    public void testGetFileC() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetFileC1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-03-20", mf1);
        MyFile newf1 = c1.getFile();
        assertEquals("testGetFileC1", newf1.getName());
        assertEquals("Testing", newf1.getPath());
    }
    public void testSetFileC() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testSetFileC1", "Testing");
        mf1.createFile();
        MyFile mf2 = new MyFile("testSetFileC2", "Testing2");
        mf2.createFile();
        Contract c1 = new Contract("2017-11-29",mf1);
        c1.setFile(mf2);
        MyFile newf1 = c1.getFile();
        assertEquals("testSetFileC2", newf1.getName());
        assertEquals("Testing2", newf1.getPath());
        MyFile mf3 = new MyFile("testSetFileC3", "Testing");
        boolean b2 = true;
        try{
            c1.setFile(mf3);
        }
        catch(Error e){
            b2 = false;
            assertEquals("File is not Found", e.getMessage());
        }
        assertFalse(b2);
    }
    public static void main(String[] args) throws IOException,ParseException {
        TestContract tc = new TestContract();
        tc.testContractInit();
        tc.testGetDate();
        tc.testSetDate();
        tc.testGetFileC();
        tc.testSetFileC();
    }
}
