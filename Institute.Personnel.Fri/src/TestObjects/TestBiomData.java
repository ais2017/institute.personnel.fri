/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestObjects;
import ObjectClasses.*;
import java.io.*;
import java.util.*;
import static junit.framework.Assert.*;
/**
 *
 * @author justiceforall88
 */
public class TestBiomData {
    public void testGetFileBD() throws IOException{
        MyFile mf1 = new MyFile("testGetFileBMD1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        MyFile newf1 = bm1.getFile();
        assertEquals("testGetFileBMD1", newf1.getName());
        assertEquals("Testing", newf1.getPath());
        
    }
    public void testSetFileBD() throws IOException{
        MyFile mf1 = new MyFile("testSetFileBMD1", "Testing");
        mf1.createFile();
        MyFile mf2 = new MyFile("testSetFileBMD2", "Testing2");
        mf2.createFile();
        BiomData bm1 = new BiomData(mf1);
        bm1.setFile(mf2);
        MyFile newf1 = bm1.getFile();
        assertEquals("testSetFileBMD2", newf1.getName());
        assertEquals("Testing2", newf1.getPath());
    }
    public void testCreate() throws IOException{
        MyFile mf1 = new MyFile("testCreateBMD1", "Testing");
        BiomData bm1 = new BiomData(mf1);
        bm1.create();
        File dir = new File(bm1.getFile().getPath());
        assertTrue(dir.exists());
        File f = new File(dir, bm1.getFile().getName());
        assertTrue(f.exists());
    }
    public void testDifference() throws IOException{
        MyFile mf1 = new MyFile("testDifferenceBMD1", "Testing");
        BiomData bm1 = new BiomData(mf1);
        bm1.create();
        MyFile mf2 = new MyFile("testDifferenceBMD2", "Testing");
        BiomData bm2 = new BiomData(mf2);
        bm2.create();
        String ss = "justiceforall";
        byte []b = ss.getBytes();
        mf1.changeFile(b);
        mf2.changeFile(b);
        FileInputStream fin1 = mf1.openFileR();
        int i=-1;
        List data1 = new ArrayList <Character>(); 
        while ((i=fin1.read())!=-1){
            data1.add((char)i);
        }
        String s1 = data1.toString();
        
        FileInputStream fin2 = mf2.openFileR();
        int j=-1;
        List data2 = new ArrayList <Character>(); 
        while ((j=fin2.read())!=-1){
            data2.add((char)j);
        }
        fin1.close();
        fin2.close();
        assertFalse(bm1.difference(bm2));
        
    }
    public static void main(String[] args) throws IOException {
        TestBiomData tbd = new TestBiomData();
        
        tbd.testGetFileBD();
        tbd.testSetFileBD();
        tbd.testCreate();
        tbd.testDifference();
    }
}