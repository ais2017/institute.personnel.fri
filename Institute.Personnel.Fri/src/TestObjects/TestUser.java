/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestObjects;
import ObjectClasses.*;
import java.io.*;
import static junit.framework.Assert.*;

/**
 *
 * @author justiceforall88
 */
public class TestUser {
    
    public void testUserInit(){
        boolean b1 = true;
        try{
            User user1 = new User( null, "ctghuuj", TypeUser.ADMIN);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error: login is empty", e.getMessage());
        }
        assertFalse(b1);
        boolean b2 = true;
        try{
            User user2 = new User( "", "ctghuuj", TypeUser.ADMIN);
        }
        catch(Error e){
            b2 = false;
            assertEquals("Error: login is empty", e.getMessage());
        }
        assertFalse(b2);
        boolean b3 = true;
        try{
            User user3 = new User( "myl", "ctghuuj", TypeUser.ADMIN);
        }
        catch(Error e){
            b3 = false;
            assertEquals("Error: login is too small", e.getMessage());
        }
        assertFalse(b3);
        boolean b4 = true;
        try{
            String p = null;
            User user4 = new User("mylogin", p, TypeUser.ADMIN);
        }
        catch(Error e){
            b4 = false;
            assertEquals("Error: password is empty", e.getMessage());
        }
        assertFalse(b4);
        boolean b5 = true;
        try{
            User user5 = new User("mylogin", "", TypeUser.ADMIN);
        }
        catch(Error e){
            b5 = false;
            assertEquals("Error: password is empty", e.getMessage());
        }
        assertFalse(b5);
        boolean b6 = true;
        try{
            User user6 = new User("mylogin", "buba", TypeUser.ADMIN);
        }
        catch(Error e){
            b6 = false;
            assertEquals("Error: password is too small", e.getMessage());
        }
        assertFalse(b6);
        BiomData []bm7 = null;
        BiomData []bm8 = {};
        boolean b7 = true;
        try{
            User user7 = new User("mylogin", bm7, TypeUser.ADMIN);
        }
        catch(Error e){
            b7 = false;
            assertEquals("Error: empty bmd array", e.getMessage());
        }
        assertFalse(b7);
        boolean b8 = true;
        try{
            User user8 = new User("mylogin", bm8, TypeUser.ADMIN);
        }
        catch(Error e){
            b8 = false;
            assertEquals("Error: empty bmd array", e.getMessage());
        }
        assertFalse(b8);
    }
    public void testGetLogin(){
        User user = new User("justiceforall88", "qwerty123", TypeUser.ADMIN);
        String actual = user.getLogin();
        assertEquals("justiceforall88", actual);
    }
    public void testSetLogin(){
        User user = new User("justiceforall88", "qwerty123", TypeUser.ADMIN);
        user.setLogin("VolantZ");
        String actual = user.getLogin();
        assertEquals("VolantZ", actual);
        boolean b1 = true;
        try{
            user.setLogin(null);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error: login is empty", e.getMessage());
        }
        assertFalse(b1);
        boolean b2 = true;
        try{
            user.setLogin("");
        }
        catch(Error e){
            b2 = false;
            assertEquals("Error: login is empty", e.getMessage());
        }
        assertFalse(b2);
        boolean b3 = true;
        try{
            user.setLogin("myl");
        }
        catch(Error e){
            b3 = false;
            assertEquals("Error: login is too small", e.getMessage());
        }
        assertFalse(b3);
    }
    public void testGetPassword(){
        User user = new User("justiceforall88", "qwerty123", TypeUser.ADMIN);
        String actual = user.getPassword();
        assertEquals("qwerty123", actual);
    }
    public void testSetPassword(){
        User user = new User("justiceforall88", "qwerty123", TypeUser.ADMIN);
        user.setPassword("MetallicA");
        String actual = user.getPassword();
        assertEquals("MetallicA", actual);
        boolean b4 = true;
        try{
            user.setPassword(null);
        }
        catch(Error e){
            b4 = false;
            assertEquals("Error: password is empty", e.getMessage());
        }
        assertFalse(b4);
        boolean b5 = true;
        try{
            user.setPassword("");
        }
        catch(Error e){
            b5 = false;
            assertEquals("Error: password is empty", e.getMessage());
        }
        assertFalse(b5);
        boolean b6 = true;
        try{
            user.setPassword("buba");
        }
        catch(Error e){
            b6 = false;
            assertEquals("Error: password is too small", e.getMessage());
        }
        assertFalse(b6);
    }
    public void testGetBiomDataArr() throws IOException{
        MyFile mf1 = new MyFile("testGetBiomDataArrU1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        MyFile mf2 = new MyFile("testGetBiomDataArrU2", "Testing");
        mf2.createFile();
        BiomData bm2 = new BiomData(mf2);
        MyFile mf3 = new MyFile("testGetBiomDataArrU3", "Testing");
        mf3.createFile();
        BiomData bm3 = new BiomData(mf3);
        BiomData []bm = {bm1, bm2, bm3};
        User user = new User("justiceforall88", bm, TypeUser.ADMIN);
        BiomData []actual = user.getBiomDataArr();
        assertEquals(3, actual.length);
        assertEquals(bm1, actual[0]);
        assertEquals(bm2, actual[1]);
        assertEquals(bm3, actual[2]);
    }
    public void testSetBiomDataArr() throws IOException{
        MyFile mf1 = new MyFile("testGetBiomDataArrU1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bm_start = {bm1};
        MyFile mf2 = new MyFile("testGetBiomDataArrU2", "Testing");
        mf2.createFile();
        BiomData bm2 = new BiomData(mf2);
        MyFile mf3 = new MyFile("testGetBiomDataArrU3", "Testing");
        mf3.createFile();
        BiomData bm3 = new BiomData(mf3);
        BiomData []bm_expect = {bm2, bm3};
        User user = new User("justiceforall88", bm_start, TypeUser.ADMIN);
        user.setBiomDataArr(bm_expect);
        BiomData []actual = user.getBiomDataArr();
        assertEquals(2, actual.length);
        assertEquals(bm2, actual[0]);
        assertEquals(bm3, actual[1]);
        BiomData []bm7 = null;
        BiomData []bm8 = {};
        boolean b7 = true;
        try{
            user.setBiomDataArr(bm7);
        }
        catch(Error e){
            b7 = false;
            assertEquals("Error: empty bmd array", e.getMessage());
        }
        assertFalse(b7);
        boolean b8 = true;
        try{
            user.setBiomDataArr(bm8);
        }
        catch(Error e){
            b8 = false;
            assertEquals("Error: empty bmd array", e.getMessage());
        }
        assertFalse(b8);
    }
    public void testGetType(){
        User user = new User("justiceforall88", "qwerty123", TypeUser.ADMIN);
        TypeUser actual = user.getType();
        assertEquals(TypeUser.ADMIN, actual);
    }
    public void testSetType(){
        User user = new User("justiceforall88", "qwerty123", TypeUser.ADMIN);
        user.setType(TypeUser.EMPLOK);
        TypeUser actual = user.getType();
        assertEquals(TypeUser.EMPLOK, actual);
    }
    
    public static void main(String[] args) throws IOException {
        
        TestUser tu = new TestUser();
        tu.testUserInit();
        tu.testGetLogin();
        tu.testSetLogin();
        tu.testGetPassword();
        tu.testSetPassword();
        tu.testGetBiomDataArr();
        tu.testSetBiomDataArr();
        tu.testGetType();
        tu.testSetType();

    }
}







