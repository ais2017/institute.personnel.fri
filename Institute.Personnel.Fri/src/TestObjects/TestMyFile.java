/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestObjects;
import ObjectClasses.*;
import java.io.*;
import java.util.*;
import static junit.framework.Assert.*;

/**
 *
 * @author justiceforall88
 */
public class TestMyFile {
    
    public void testMyFileInit() throws IOException{
        boolean b1 = true;
        try{
            MyFile mf1 = new MyFile(null);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error: empty name of file", e.getMessage());
        }
        assertFalse(b1);
        boolean b2 = true;
        try{
            MyFile mf2 = new MyFile("");
        }
        catch(Error e){
            b2 = false;
            assertEquals("Error: empty name of file", e.getMessage());
        }
        assertFalse(b2);
        boolean b3 = true;
        try{
            MyFile mf3 = new MyFile(null, "asd");
        }
        catch(Error e){
            b3 = false;
            assertEquals("Error: empty name of file", e.getMessage());
        }
        assertFalse(b3);
        boolean b4 = true;
        try{
            MyFile mf4 = new MyFile("", "fwef");
        }
        catch(Error e){
            b4 = false;
            assertEquals("Error: empty name of file", e.getMessage());
        }
        assertFalse(b4);
    }
    public void testGetName(){
        MyFile mf1 = new MyFile("testGetNameMF1", "Testing");
        assertEquals("testGetNameMF1", mf1.getName());
    }
    public void testSetName() throws IOException{
        MyFile mf0 = new MyFile("testSetNameMF1", "Testing");
        mf0.setName("testSetNameMF123");
        assertEquals("testSetNameMF123", mf0.getName());
        boolean b1 = true;
        try{
            mf0.setName(null);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error: empty name of file", e.getMessage());
        }
        assertFalse(b1);
        boolean b2 = true;
        try{
            mf0.setName("");
        }
        catch(Error e){
            b2 = false;
            assertEquals("Error: empty name of file", e.getMessage());
        }
        assertFalse(b2);
    }
    public void testGetPath(){
        MyFile mf1 = new MyFile("testGetPathMF1", "Testing");
        assertEquals("Testing", mf1.getPath());
    }
    public void testSetPath(){
        MyFile mf1 = new MyFile("testSetPathMF1", "Testing");
        mf1.setPath("Testing2");
        assertEquals("Testing2", mf1.getPath());
    }
    public void testCreateFile() throws IOException{
        MyFile mf1 = new MyFile("testCreateFileMF1", "Testing");
        mf1.createFile();
        File dir = new File(mf1.getPath());
        assertTrue(dir.exists());
        File f = new File(dir, mf1.getName());
        assertTrue(f.exists());

    }
    public void testOpenFileR() throws IOException{
        String expected = "cherep";
        byte []b_expected = expected.getBytes();
        MyFile mf1 = new MyFile("testOpenFileRMF1", "Testing");
        mf1.createFile();
        File file = new File(mf1.getPath(), mf1.getName());
        FileOutputStream fos1 = new FileOutputStream(file);
        fos1.write(b_expected);
        fos1.close();
        FileInputStream fin1 = mf1.openFileR();
        int i=-1;
        String actual = "";
        while ((i=fin1.read())!=-1){
            actual = actual + Character.toString((char)i);
        }
        fin1.close();
        assertEquals(expected,actual);
        boolean b1 = true;
        try{
            MyFile mf2 = new MyFile("testOpenFileRMF2", "Testing");
            FileInputStream fin2 = mf2.openFileR();
        }
        catch(Error e){
            b1 = false;
            assertEquals("File doesn't exist", e.getMessage());
        }
        assertFalse(b1);
    }
    public void testOpenFileW() throws IOException{
        String expected = "mozambik";
        byte []b_expected = expected.getBytes();
        MyFile mf1 = new MyFile("testOpenFileWMF1", "Testing");
        mf1.createFile();
        FileOutputStream fos1 = mf1.openFileW();
        fos1.write(b_expected);
        fos1.close();
        File file = new File(mf1.getPath(), mf1.getName());
        FileInputStream fin1 = new FileInputStream(file);
        int i=-1;
        String actual = "";
        while ((i=fin1.read())!=-1){
            actual = actual + Character.toString((char)i);
        }
        fin1.close();
        assertEquals(expected,actual);
        boolean b1 = true;
        try{
            MyFile mf2 = new MyFile("testOpenFileWMF2", "Testing");
            FileOutputStream fos2 = mf2.openFileW();
        }
        catch(Error e){
            b1 = false;
            assertEquals("File doesn't exist", e.getMessage());
        }
        assertFalse(b1);
    }
    public void testChangeFile() throws IOException{
        String start = "igoryok";
        byte []b_start = start.getBytes();
        String expected = "petushok";
        byte []b_expected = expected.getBytes();
        MyFile mf1 = new MyFile("testChangeFileMF1", "Testing");
        mf1.createFile();
        FileOutputStream fos1 = mf1.openFileW();
        fos1.write(b_expected);
        fos1.close();
        mf1.changeFile(b_expected);
        FileInputStream fin1 = mf1.openFileR();
        int i=-1;
        String actual = "";
        while ((i=fin1.read())!=-1){
            actual = actual + Character.toString((char)i);
        }
        fin1.close();
        assertEquals(expected,actual);
    }
    public static void main(String[] args) throws IOException {
        TestMyFile tmf = new TestMyFile();
        tmf.testMyFileInit();
        tmf.testGetName();
        tmf.testSetName();
        tmf.testGetPath();
        tmf.testSetPath();
        tmf.testCreateFile();
        tmf.testOpenFileR();
        tmf.testOpenFileW();
        tmf.testChangeFile();
        
    }
}
