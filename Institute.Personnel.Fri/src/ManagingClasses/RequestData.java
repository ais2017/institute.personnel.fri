/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagingClasses;
import ObjectClasses.*;
import java.text.ParseException;
import java.util.*;
import DBClasses.*;
/**
 *
 * @author justiceforall88
 */

public class RequestData {
    private Person pd_params;
    
    public RequestData(Person params){
        pd_params = params;
    }
    
    public List<Person> pd = new ArrayList<Person>();

    public RequestData(){}
    public void setPDParams(Person p){
        pd_params=p;
    }
    public Person getPDParams(){return pd_params;}
    
    public void fillingData(){
        createBaseRequestWindow();
    }
    
    public void checkAccess(){
        changeRequestWindow();
    }

    public void inputData(Person pp){ 
        inputInterfaceData(pp);
    }
    
    public void checkCorrect() throws ParseException{
        pd_params.checkCorrectData();      
    }
    
    public void queryToDB(){
        Persons p = new Persons_Gateway();
        pd = p.select_params(pd_params);
    }
    
    public List<Person> orgDataForSinEmpl(){
        if (pd.size() > 1)
            throw new Error("Error: too much person!");
        else if (pd.isEmpty()){
            throw new Error("Error: person is not found!");
        }
        else if (pd.size() == 1){ 
            outputDataSE();
            return pd;
        }
        return null;
    }
    
    public List<Person> orgDataForMulEmpl(){
        if (pd.isEmpty())
            throw new Error("Error: person is not found!");
        else if (pd.size() >= 1){
            outputDataME();
            return pd;
        }
        return null;
    }
    
    void createBaseRequestWindow(){
        //
    }
    void changeRequestWindow(){
        //
    }
    void inputInterfaceData(Person pp){
        pd_params = pp; 
    }
    void outputDataSE(){
        //
    }
    void outputDataME(){
        //
    }
    
}
