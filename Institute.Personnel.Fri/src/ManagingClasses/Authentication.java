/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagingClasses;
import ObjectClasses.*;
import java.io.IOException;
import java.util.*;
import DBClasses.*;
/**
 *
 * @author justiceforall88
 */
public class Authentication {
    public enum TypeAuth{PWD,BIOM_DATA};
    private TypeAuth tpAuth;
    private String input_log = null;
    private String input_pwd = null;
    private BiomData input_bmd = null;
   
    public Authentication(TypeAuth tp) {
        tpAuth = tp;
    }
  
    public Authentication(String l, String p) {
        tpAuth = TypeAuth.PWD;
        input_log = l;
        input_pwd = p;
    }
    public Authentication(String l, BiomData b) {
        tpAuth = TypeAuth.BIOM_DATA;
        input_log = l;
        input_bmd = b;
    }
    public TypeAuth getTypeAuth(){return tpAuth;}
    public void setTypeAuth(TypeAuth ta){tpAuth = ta;} 
    
    public String getLogin(){return input_log;}
    public void setLogin(String l){input_log = l;}
    
    public String getPassword(){return input_pwd;}
    public void setPassword(String p){input_pwd = p;}
    
    public BiomData getBiomData(){return input_bmd;}
    public void setBiomData(BiomData b){input_bmd = b;}
    
    public void fillDataforAuthLP(String l, String p){
        if (tpAuth == TypeAuth.PWD){
            input_log = fillLogin(l);
            input_pwd = fillPassword(p);
            String[] res = {input_log,input_pwd};
            if (res.length != 2)
                throw new Error("Error with Log-Pwd Authentication");

        }
        else
            throw new Error("Error: This methog is for Log-Pwd Authentication");
    }
    public void fillDataforAuthBD(String l, BiomData b) throws IOException{
        if (tpAuth == TypeAuth.BIOM_DATA){
            input_log = fillLogin(l);
            input_bmd = fillBmD(b);
            Object[] res = {input_log,input_bmd};
            if (res.length != 2)
                throw new Error("Error with Log-Bmd Authentication");
        }
        else
            throw new Error("Error: This methog is for Log-Bmd Authentication");
    }
    
    public boolean findID(){
        return isFound();
    }
    public BiomData getBiomDataFromInt(String n, String p) throws IOException{
        return new BiomData(inputBiomData(n,p));
    }
    public boolean isPassMatch(){
        
        return isPM(input_log);
    }
    MyFile inputBiomData(String n, String p) throws IOException{
        MyFile f = new MyFile(n,p);
        f.createFile();
        return f;
    }
    
    public boolean isBmdMatch() throws IOException{
        Users u = new Users_Gateway();
        Object[] pb = u.select(input_log);
        if ((pb == null) || (pb.length <3))
            throw new Error("Error: user data is not enough");
        if (pb.length>3){
            throw new Error("Error: there are excess data");
        }
        BiomData []bm = (BiomData[]) pb[1];
        if (input_bmd!=null){
            for (BiomData bm1 : bm) {
                
                if (!bm1.difference(input_bmd)) {
                    return true;
                }  
            }
        }
        return false;          
    }
    String fillLogin(String log){
        
        return log;
    }
    
    String fillPassword(String pwd){
        
        return pwd;
    }
    
    BiomData fillBmD(BiomData bmd){
        
        return bmd;
    }
    
    boolean isFound(){
        if (input_log == null)
            throw new Error("Error: empty login");
        Users u = new Users_Gateway();
        if (u.select(input_log)!=null)
            return true;
        
        return false;
    }
    boolean isPM(String log){
        Users u = new Users_Gateway();
        Object[] pb = u.select(log);
        if (pb == null)
            throw new Error("Error: problem with finding login");
        if (pb.length <3)
            throw new Error("Error: user data is not enough");
        if (pb.length>3){
            throw new Error("Error: there are excess data");
        }
        String pass = (String) pb[0];
        return (pass.equals(input_pwd)); 
    }
}
