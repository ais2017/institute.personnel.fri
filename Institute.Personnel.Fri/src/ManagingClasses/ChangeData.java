/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagingClasses;
import ObjectClasses.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import DBClasses.*;
/**
 *
 * @author justiceforall88
 */
public class ChangeData {
    private Contract[] n_contrs = null;
    private MyFile[] n_files = null;
    private String n_Post = null;
    private String n_Subdiv = null;
    private String n_scRank = null;
    private String n_scGrade = null;
    private String []n_fio = null;
    
    
    public ChangeData(){}
    public ChangeData(String []nfio, String npost, String nsubdiv, String nscrank, 
            String nscgrage, MyFile[] nmfls, Contract[] ncnts){
        if ((nfio!=null)&&(nfio.length==3))
            n_fio = Arrays.copyOf(nfio, nfio.length);
        else
            n_fio = null;
        n_Post = npost;
        n_Subdiv = nsubdiv;
        n_scRank = nscrank;
        n_scGrade = nscgrage;
        if (nmfls!=null)
            n_files = Arrays.copyOf(nmfls, nmfls.length);
        else
            n_files = null;
        if (ncnts!=null)
            n_contrs = Arrays.copyOf(ncnts, ncnts.length);
        else
            n_contrs = null;
        
    }
    
    public void setNFIO(String []fio){
        int len = fio.length;
        if (len>0){
            n_fio = Arrays.copyOf(fio, len);
        }
        else
            throw new Error("FIO is empty");
        
    }
    public String[] getNFIO(){return Arrays.copyOf(n_fio, n_fio.length);}
 

    public void setNPost(String npost){n_Post = npost;}
    public String getNPost(){return n_Post;}
    public void setNSubdiv(String nsub){n_Subdiv = nsub;}
    public String getNSubdiv(){return n_Subdiv;}
    public void setNScRank(String nscr){n_scRank = nscr;}
    public String getNScRank(){return n_scRank;}
    public void setNScGrade(String nscr){n_scGrade = nscr;}
    public String getNScGrade(){return n_scGrade;}
    public void setNContrs(Contract []cnt){
        int len = cnt.length;
        if (len>0){
            n_contrs = Arrays.copyOf(cnt, len);
        }
        else
            throw new Error("Contract array is empty");
        
    }
    public Contract[] getNContrs(){return Arrays.copyOf(n_contrs, n_contrs.length);}
    public void setNFiles(MyFile []mfl){
        int len = mfl.length;
        if (len>0){
            n_files = Arrays.copyOf(mfl, len);
        }
        else
            throw new Error("MyFile array is empty");
        
    }
    public MyFile[] getNFiles(){return Arrays.copyOf(n_files, n_files.length);}
    
    public Person fillData(User user, String []nfio, String npost, 
            String nsubdiv, String nscrank, String nscgrade,
            MyFile []nfs, Contract[] ncs) throws IOException, ParseException{ 
        if (isUserTheRight(user)){
            
            inputData(nfio, npost, nsubdiv, nscrank, nscgrade, nfs, ncs);
            Person newPD = new Person(n_fio, n_Post, n_Subdiv, n_scRank, n_scGrade, 
                    n_files, n_contrs);
            return newPD;
            
        }
        else
            throw new Error("Error with access rights");
    }
    public boolean isUserTheRight(User user){
        if ((user.getType() == TypeUser.ADMIN)||(user.getType() == TypeUser.EMPLOK)){
            return true;
        }
        else
            return false;
    }
    
    public boolean isUserTheFullRight(User user){
        if (user.getType() == TypeUser.ADMIN){
            return true;
        }
        else
            return false;
    }
    public boolean makeChanges(User user, Person pd, int id) throws ParseException{
        
        if (isUserTheRight(user)){
            Persons p = new Persons_Gateway();
            if (pd.getFIO().length < 2 || pd.getFIO().length > 3)
                throw new Error("Incomplete Person Data: Wrong number of FIO String");
            if (pd.getPost() == null)
                throw new Error("Incomplete Person Data: Empty Post");
            pd.checkCorrectData();
            boolean b = p.update(id, pd);
            return b;
        }
        else
            throw new Error("Error with access rights");
        
    }
    
    public boolean makeAdd(User user, Person pd, int id) throws ParseException{
        
        if (isUserTheFullRight(user)){
            Persons p = new Persons_Gateway();
            if (pd.getFIO().length < 2 || pd.getFIO().length > 3)
                throw new Error("Incomplete Person Data: Wrong number of FIO String");
            if (pd.getPost() == null)
                throw new Error("Incomplete Person Data: Empty Post");
            pd.checkCorrectData();
            boolean b = p.insert(id, pd);
            return b;
        }
        else
            throw new Error("Error with access rights");
        
    }
    
    public boolean makeDel(User user, int id) throws ParseException{
        
        if (isUserTheFullRight(user)){
            Persons p = new Persons_Gateway();
            boolean b = p.delete(id);
            return b;
        }
        else
            throw new Error("Error with access rights");
        
    }
    
    public boolean addPost(User user, String s){
        if (isUserTheFullRight(user)){
            List_Posts p = new List_Posts_Gateway();
            boolean b = p.insert(s);
            return b;
        }
        else
            throw new Error("Error with access rights");
    }
    public boolean addSubdiv(User user, String s){
        if (isUserTheFullRight(user)){
            List_Subdivs p = new List_Subdivs_Gateway();
            boolean b = p.insert(s);
            return b;
        }
        else
            throw new Error("Error with access rights");
    }
    public boolean addScRank(User user, String s){
        if (isUserTheFullRight(user)){
            List_ScRanks p = new List_ScRanks_Gateway();
            boolean b = p.insert(s);
            return b;
        }
        else
            throw new Error("Error with access rights");
    }
    public boolean addScGrade(User user, String s){
        if (isUserTheFullRight(user)){
            List_ScGrades p = new List_ScGrades_Gateway();
            boolean b = p.insert(s);
            return b;
        }
        else
            throw new Error("Error with access rights");
    }
    public boolean changePost(User user, int id, String s){
        if (isUserTheFullRight(user)){
            List_Posts p = new List_Posts_Gateway();
            boolean b = p.update(id, s);
            return b;
        }
        else
            throw new Error("Error with access rights");
    }
    public boolean changeSubdiv(User user, int id, String s){
        if (isUserTheFullRight(user)){
            List_Subdivs p = new List_Subdivs_Gateway();
            boolean b = p.update(id, s);
            return b;
        }
        else
            throw new Error("Error with access rights");
    }
    public boolean changeScRank(User user, int id, String s){
        if (isUserTheFullRight(user)){
            List_ScRanks p = new List_ScRanks_Gateway();
            boolean b = p.update(id, s);
            return b;
        }
        else
            throw new Error("Error with access rights");
    }
    public boolean changeScGrade(User user, int id, String s){
        if (isUserTheFullRight(user)){
            List_ScGrades p = new List_ScGrades_Gateway();
            boolean b = p.update(id, s);
            return b;
        }
        else
            throw new Error("Error with access rights");
    }
    public boolean deletePost(User user, int id){
        if (isUserTheFullRight(user)){
            List_Posts p = new List_Posts_Gateway();
            boolean b = p.delete(id);
            return b;
        }
        else
            throw new Error("Error with access rights");
    }
    public boolean deleteSubdiv(User user, int id){
        if (isUserTheFullRight(user)){
            List_Subdivs p = new List_Subdivs_Gateway();
            boolean b = p.delete(id);
            return b;
        }
        else
            throw new Error("Error with access rights");
    }
    public boolean deleteScRank(User user, int id){
        if (isUserTheFullRight(user)){
            List_ScRanks p = new List_ScRanks_Gateway();
            boolean b = p.delete(id);
            return b;
        }
        else
            throw new Error("Error with access rights");
    }
    public boolean deleteScGrade(User user, int id){
        if (isUserTheFullRight(user)){
            List_ScGrades p = new List_ScGrades_Gateway();
            boolean b = p.delete(id);
            return b;
        }
        else
            throw new Error("Error with access rights");
    }
    public void inputData(String []nfio, String npost, 
            String nsubdiv, String nscrank, String nscgrade,
            MyFile []nfs, Contract[] ncs) throws IOException{
       
        n_fio = getFIOFromIntf(nfio);
        n_Post = getPostFromIntf(npost);
        n_Subdiv = getSubdivFromIntf(nsubdiv);
        n_scRank = getScRankFromIntf(nscrank);
        n_scGrade = getScGradeFromIntf(nscgrade);
        n_files = getFilesFromIntf(nfs);
        n_contrs = getContractsFromIntf(ncs);
    }
    public boolean isCorrect(Person pd) throws ParseException{
        boolean b = true; 
        try{
            pd.checkCorrectData();
        }
        catch(Error e){
            b = false;
        }
        return b;
    }
    String[] getFIOFromIntf(String []fio){
        return Arrays.copyOf(fio, fio.length);
    }

    String getPostFromIntf(String s){
        return s;
    }
    String getSubdivFromIntf(String s){
        return s;
    }
    String getScRankFromIntf(String s){
        return s;
    }
    String getScGradeFromIntf(String s){
        return s;
    }
    MyFile[] getFilesFromIntf (MyFile[] mfs){
        return Arrays.copyOf(mfs, mfs.length);
    }
    Contract[] getContractsFromIntf(Contract[] cts){
        return Arrays.copyOf(cts, cts.length);
    }
}
