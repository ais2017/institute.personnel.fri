/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagingClasses;
import ObjectClasses.*;
import java.io.*;
import java.util.*;
import DBClasses.*;
/**
 *
 * @author justiceforall88
 */
public class Registration {
    private Application appl = new Application();//только что созданная заявка
    
    public Registration(){
    }
    public Registration(Application a){
        appl = a;
    }
    public void setAppl(Application a){appl = a;}
    public Application getAppl(){return appl;}
    public void fillAppl(String l, String p1, String p2, BiomData[] biomdata, String id, TypeUser tt) 
            throws IOException{
        
        String login = getLoginFromIntf(l);
        
        if ((login==null)||(login.equals(""))){
            appl.setStatus(ApplStat.ERROR);//ошибка
            throw new Error("Error: log is required field");
        }
        if (login.length() < 6){
            appl.setStatus(ApplStat.ERROR);//ошибка
            throw new Error("Error: log is too small");
        }
        
        String password1  = getPassword1FromIntf(p1);
        
        if ((password1==null)||(password1.equals(""))){
            appl.setStatus(ApplStat.ERROR);//ошибка
            throw new Error("Error: pas is required field");
        }
        if (password1.length() < 6){
            appl.setStatus(ApplStat.ERROR);//ошибка
            throw new Error("Error: pas is too small");
        }
        
        String password2  = getPassword2FromIntf(p2);
        
        if (!password1.equals(password2)){
            appl.setStatus(ApplStat.ERROR);//ошибка
            throw new Error("Error: password don't match");
        }

        int idWork = Integer.parseInt(getIdFromIntf(id));
        
        if ((idWork<=9999) || (idWork>=100000)){
            appl.setStatus(ApplStat.ERROR);//ошибка
            throw new Error("Error: invalid idWork");
        }
        
        TypeUser t = getTypeUserFromIntf(tt);

        BiomData []bmd = getBiomDataArrFromIntf(biomdata, l);
        
        if ((bmd==null)||(bmd.length<1)){
            appl.setStatus(ApplStat.ERROR);//ошибка
            throw new Error("Error: BiomData array is empty");
        }
        
        appl.fillApplForReg(login, password1, idWork, t, bmd);
        appl.setStatus(ApplStat.FILL_BUT_NOT_CHECK);//заполнена, но непроверена
    }
    public boolean isCorrectData(boolean bb){
        boolean b = answAdmin(bb);
        if (b)
           appl.setStatus(ApplStat.ACCEPTED);//успешно проверена
        else
           appl.setStatus(ApplStat.REJECTED);//есть некорректные данные
        return b;
    }
    public boolean answAdmin(boolean b){
        return b;
    }
    public void introUserToSystem(boolean b){
        if (isCorrectData(b))
            addNewUserToDB();
        else
            throw new Error("No access!");
    }
    public void transfDataForLoginToSys(){
        openSystemforUser();
    }
    void addNewUserToDB(){
        //;
        Object []ob = {appl.getPassword(), appl.getBiomDataArr(), appl.getTypeUser()};
        Users u = new Users_Gateway();
        u.insert(appl.getLogin(), ob);
    }
    void openSystemforUser(){
        //;
    }
    String getLoginFromIntf(String s){
        return s;
    }
    String getPassword1FromIntf(String s){
        return s;
    }
    String getPassword2FromIntf(String s){
        return s;
    }
    String getIdFromIntf(String s){
        return s;
    }
    TypeUser getTypeUserFromIntf(TypeUser t){
        return t;
    }
    BiomData[] getBiomDataArrFromIntf(BiomData []barr, String l) throws IOException{
        if ((barr!=null)&&(barr.length > 0))
            appl.setBiomDataArr(Arrays.copyOf(barr, barr.length));
        else{
            appl.setBiomDataArr(null);
            return appl.getBiomDataArr();
        }
        for (int i=0;i<barr.length;i++){
            BiomData b = appl.getBiomDataArr()[i];
            MyFile newf = b.getFile();
            newf.createFile();
            newf.changeFile(l.getBytes());
        }
        return Arrays.copyOf(barr, barr.length);
    }
    
}
