/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestDB;
import DBClasses.*;
import static junit.framework.Assert.*;

/**
 *
 * @author justiceforall88
 */
public class TestListPosts {
//        private static List<String> Posts = new ArrayList<>(Arrays.asList("Ректор", "Заместитель ректора",
//        "Руководитель структурного подразделения","Начальник отдела аспирантуры",
//        "Советник при ректорате", "Ассистент", "Преподаватель", "Старший преподаватель",
//        "Доцент", "Ученый секретарь совета учреждения", "Помощник ректора",
//    "Профессор", "Заведующий кафедрой", "Заместитель заведующего кафедрой", "Декан факультета",
//    "Заместитель декана факультета", "Диспетчер факультета", "Тьютор", 
//    "Специалист по учебно-методической работе", "Учебный мастер"));
    public void testSelect(){
        List_Posts p = new List_Posts_Gateway();
        assertEquals(0, p.select("Ректор"));
        assertEquals(-1, p.select("Строитель"));
    }
    public void testInsert(){
        List_Posts p = new List_Posts_Gateway();
        boolean b1 = p.insert("Прораб");
        assertTrue(b1);
        boolean b2 = p.insert("Прораб");
        assertFalse(b2);
    }
    public void testUpdate(){
        List_Posts p = new List_Posts_Gateway();
        assertEquals(1, p.select("Заместитель ректора"));
        boolean b1 = p.update(1,"Директор");
        assertTrue(b1);
        assertEquals(1, p.select("Директор"));
        boolean b2 = p.update(3,"Директор");
        assertFalse(b2);
        assertEquals(1, p.select("Директор"));
        boolean b3 = p.update(100,"Астроном");
        assertFalse(b3);
        assertEquals(1, p.select("Директор"));
    }
    public void testDelete(){
        List_Posts p = new List_Posts_Gateway();
        assertEquals(3, p.select("Начальник отдела аспирантуры"));
        boolean b1 = p.delete(3);
        assertTrue(b1);
        assertEquals(-1, p.select("Начальник отдела аспирантуры"));
        boolean b2 = p.delete(3);
        assertFalse(b2);
        assertEquals(-1, p.select("Начальник отдела аспирантуры"));
    }
    public void testClear(){
        List_Posts p = new List_Posts_Gateway();
        p.clear();
        assertEquals(0, p.getAll().size());
    }
    public static void main(String[] args) {
        TestListPosts tlp = new TestListPosts();
        
        tlp.testSelect();
        tlp.testInsert();
        tlp.testUpdate();
        tlp.testDelete();
        tlp.testClear();
    }
}
