/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestDB;
import DBClasses.*;
import static junit.framework.Assert.*;
/**
 *
 * @author justiceforall88
 */
public class TestListSubdivs {
//    private static List<String> Subdivs = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H"));
    public void testSelect(){
        List_Subdivs p = new List_Subdivs_Gateway();
        assertEquals(0, p.select("A"));
        assertEquals(-1, p.select("Z"));
    }
    public void testInsert(){
        List_Subdivs p = new List_Subdivs_Gateway();
        boolean b1 = p.insert("I");
        assertTrue(b1);
        boolean b2 = p.insert("I");
        assertFalse(b2);
    }
    public void testUpdate(){
        List_Subdivs p = new List_Subdivs_Gateway();
        assertEquals(1, p.select("B"));
        boolean b1 = p.update(1,"P");
        assertTrue(b1);
        assertEquals(1, p.select("P"));
        boolean b2 = p.update(3,"P");
        assertFalse(b2);
        assertEquals(1, p.select("P"));
        boolean b3 = p.update(100,"Y");
        assertFalse(b3);
        assertEquals(1, p.select("P"));
    }
    public void testDelete(){
        List_Subdivs p = new List_Subdivs_Gateway();
        assertEquals(3, p.select("D"));
        boolean b1 = p.delete(3);
        assertTrue(b1);
        assertEquals(-1, p.select("D"));
        boolean b2 = p.delete(3);
        assertFalse(b2);
        assertEquals(-1, p.select("D"));
    }
    public void testClear(){
        List_Subdivs p = new List_Subdivs_Gateway();
        p.clear();
        assertEquals(0, p.getAll().size());
    }
    public static void main(String[] args) {
        TestListSubdivs tls = new TestListSubdivs();
        
        tls.testSelect();
        tls.testInsert();
        tls.testUpdate();
        tls.testDelete();
        tls.testClear();
    }
}
