/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestDB;
import DBClasses.*;
import ObjectClasses.*;
import java.io.IOException;
import java.util.Arrays;
import static junit.framework.Assert.*;
/**
 *
 * @author justiceforall88
 */
public class TestUsers {
    User u1, u2,u3;
    String l1,l2,l3;
    Object [] obj1,obj2,obj3;
    void init() throws IOException{
        MyFile mf1 = new MyFile("testGetBiomDataArrU1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bmd1 = {bm1};
        l1 = "justiceforall88";
        String p1 = "myqwerty123";
        MyFile mf2 = new MyFile("testGetBiomDataArrU2", "Testing");
        mf2.createFile();
        BiomData bm2 = new BiomData(mf2);
        BiomData []bmd2 = {bm2};
        l2 = "matvey43";
        String p2 = "catcat44";
        MyFile mf3 = new MyFile("testGetBiomDataArrU3", "Testing");
        mf3.createFile();
        BiomData bm3 = new BiomData(mf3);
        BiomData []bmd3 = {bm3};
        l3 = "ZininK96";
        String p3 = "abrakadabra";
        User user1 = new User(l1,p1, bmd1, TypeUser.ADMIN);
        User user2 = new User(l2,p2, bmd2, TypeUser.EMPLOK);
        User user3 = new User(l3,p3, bmd3, TypeUser.EMPLADM);
        Object [] o1 = {p1, bmd1, TypeUser.ADMIN};
        Object [] o2 = {p2, bmd2, TypeUser.EMPLOK};
        Object [] o3 = {p3, bmd3, TypeUser.EMPLADM};
        obj1 = o1;
        obj2 = o2;
        obj3 = o3;
        Users p = new Users_Gateway();
        p.insert(l1, obj1);
        p.insert(l2, obj2);
        p.insert(l3, obj3);
    }
    public void testSelect(){
        Users p = new Users_Gateway();
        assertTrue(Arrays.equals(obj1, p.select(l1)));
        assertFalse(Arrays.equals(obj2, p.select(l1)));
        assertEquals(null, p.select("lololo"));
    }
    public void testInsert() throws IOException{
        Users p = new Users_Gateway();
        MyFile mf1 = new MyFile("testGetBiomDataArrU1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bmd = {bm1};
        String l = "VasyaVV";
        String pp = "myqwerthj21";
        Object [] o = {pp, bmd, TypeUser.ADMIN};
        boolean b1 = p.insert(l, o);
        assertTrue(b1);
        boolean b2 = p.insert(l1, o);
        assertFalse(b2);

    }
    public void testUpdate() throws IOException{
        Users p = new Users_Gateway();
        MyFile mf1 = new MyFile("testGetBiomDataArrU1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        BiomData []bmd = {bm1};
        String l = "BobikX";
        String pp = "Ewdgd331";
        Object [] o = {pp, bmd, TypeUser.ADMIN};
        boolean b1 = p.update(l2, o);
        assertTrue(b1);
        boolean b2 = p.update(l, o);
        assertFalse(b2);
    }
    public void testDelete(){
        Users p = new Users_Gateway();
        Object []old = p.select(l3);
        assertTrue(Arrays.equals(obj3, old));;
        boolean b1 = p.delete(l3);
        assertTrue(b1);
        Object []neww = p.select(l3);
        assertEquals(null, neww);
        boolean b2 = p.delete(l3);
        assertFalse(b2);
    }
    public void testClear(){
        Users p = new Users_Gateway();
        p.clear();
        assertEquals(0, p.getAll().size());
    }
    public static void main(String[] args) throws IOException {
        TestUsers tu = new TestUsers();
        tu.init();
        tu.testSelect();
        tu.testInsert();
        tu.testUpdate();
        tu.testDelete();
        tu.testClear();
    }
}
