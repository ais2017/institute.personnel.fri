/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestDB;
import DBClasses.*;
import static junit.framework.Assert.*;

/**
 *
 * @author justiceforall88
 */
public class TestListScRanks {
//    private static List<String> ScRanks = new ArrayList<>(Arrays.asList("Доцент", "Профессор"));
   public void testSelect(){
        List_ScRanks p = new List_ScRanks_Gateway();
        assertEquals(0, p.select("Доцент"));
        assertEquals(-1, p.select("Бакалавр"));
    }
    public void testInsert(){
        List_ScRanks p = new List_ScRanks_Gateway();
        boolean b1 = p.insert("Магистр");
        assertTrue(b1);
        boolean b2 = p.insert("Магистр");
        assertFalse(b2);
    }
    public void testUpdate(){
        List_ScRanks p = new List_ScRanks_Gateway();
        assertEquals(1, p.select("Профессор"));
        boolean b1 = p.update(1,"Аспирант");
        assertTrue(b1);
        assertEquals(1, p.select("Аспирант"));
        boolean b2 = p.update(3,"Аспирант");
        assertFalse(b2);
        assertEquals(1, p.select("Аспирант"));
        boolean b3 = p.update(100,"Доктор");
        assertFalse(b3);
        assertEquals(1, p.select("Аспирант"));
    }
    public void testDelete(){
        List_ScRanks p = new List_ScRanks_Gateway();
        assertEquals(0, p.select("Доцент"));
        boolean b1 = p.delete(0);
        assertTrue(b1);
        assertEquals(-1, p.select("Доцент"));
        boolean b2 = p.delete(0);
        assertFalse(b2);
        assertEquals(-1, p.select("Доцент"));
    }
    public void testClear(){
        List_ScRanks p = new List_ScRanks_Gateway();
        p.clear();
        assertEquals(0, p.getAll().size());
    }
    public static void main(String[] args) {
        TestListScRanks tlsr = new TestListScRanks();
        
        tlsr.testSelect();
        tlsr.testInsert();
        tlsr.testUpdate();
        tlsr.testDelete();
        tlsr.testClear();
    }
}
