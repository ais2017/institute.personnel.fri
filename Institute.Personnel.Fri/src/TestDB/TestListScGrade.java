/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestDB;
import DBClasses.*;
import static junit.framework.Assert.*;

/**
 *
 * @author justiceforall88
 */
public class TestListScGrade {
//        private static List<String> ScGrades = new ArrayList<>(Arrays.asList("к.т.н.", "д.т.н.", "к.ф.-м.н.", "д.ф.-м.н.", "к.х.н.",
//        "д.х.н.", "к.э.н.","д.э.н.","к.и.н.","д.и.н.", "к.п.н.", "д.п.н."));
   public void testSelect(){
        List_ScGrades p = new List_ScGrades_Gateway();
        assertEquals(0, p.select("к.т.н."));
        assertEquals(-1, p.select("к.г.н."));
    }
    public void testInsert(){
        List_ScGrades p = new List_ScGrades_Gateway();
        boolean b1 = p.insert("к.г.н.");
        assertTrue(b1);
        boolean b2 = p.insert("к.г.н.");
        assertFalse(b2);
    }
    public void testUpdate(){
        List_ScGrades p = new List_ScGrades_Gateway();
        assertEquals(1, p.select("д.т.н."));
        boolean b1 = p.update(1,"д.ш.н.");
        assertTrue(b1);
        assertEquals(1, p.select("д.ш.н."));
        boolean b2 = p.update(3,"д.ш.н.");
        assertFalse(b2);
        assertEquals(1, p.select("д.ш.н."));
        boolean b3 = p.update(100,"д.у.н.");
        assertFalse(b3);
        assertEquals(1, p.select("д.ш.н."));
    }
    public void testDelete(){
        List_ScGrades p = new List_ScGrades_Gateway();
        assertEquals(3, p.select("д.ф.-м.н."));
        boolean b1 = p.delete(3);
        assertTrue(b1);
        assertEquals(-1, p.select("д.ф.-м.н."));
        boolean b2 = p.delete(3);
        assertFalse(b2);
        assertEquals(-1, p.select("д.ф.-м.н."));
    }
    public void testClear(){
        List_ScGrades p = new List_ScGrades_Gateway();
        p.clear();
        assertEquals(0, p.getAll().size());
    }
    public static void main(String[] args) {
        TestListScGrade tlsg = new TestListScGrade();
        
        tlsg.testSelect();
        tlsg.testInsert();
        tlsg.testUpdate();
        tlsg.testDelete();
        tlsg.testClear();
    }
}
