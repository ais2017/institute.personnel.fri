/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestDB;
import DBClasses.*;
import ObjectClasses.Contract;
import ObjectClasses.MyFile;
import ObjectClasses.Person;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import static junit.framework.Assert.*;
/**
 *
 * @author justiceforall88
 */
public class TestPersons {
    Person p1, p2,p3;
    void init() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetContractsPD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetContractsPD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetContractsPD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetContractsPD4", "Testing");
        mf4.createFile();
        MyFile []mf_1 = {mf3};
        MyFile []mf_2 = {mf4};
        String []fio1 = {"Булычев","Михаил","Сергеевич"};
        p1 = new Person(fio1, "Начальник отдела аспирантуры", null , null, "к.т.н.");
        String []fio2 = {"Янов","Александр","Сергеевич"};
        p2 = new Person(fio2, "Преподаватель", "D", null, null, mf_1, null);
        String []fio3 = {"Герасимов","Сергей","Николаевич"};
        p3 = new Person(fio3, "Доцент", "E", "Доцент", "к.т.н.", mf_2, c);
        Persons p = new Persons_Gateway();
        p.insert(0, p1);
        p.insert(1, p2);
        p.insert(2, p3);
    }
    public void testSelect1() throws ParseException{
        Persons p = new Persons_Gateway();
        assertEquals(0, p.select(p1));
        String []fio2 = {"Петров","Антон","Алексеевич"};
        Person pp = new Person(fio2, "Начальник отдела аспирантуры", null , null, "к.т.н.");
        assertEquals(-1, p.select(pp));
    }
    public void testSelect2() throws ParseException{
        Persons p = new Persons_Gateway();
        assertEquals(p1, p.select(0));
        assertEquals(null, p.select(7));
    }
    public void testSelectParams() throws ParseException{
        Persons p = new Persons_Gateway();
        Person pp1 = new Person(null, null, null , null, "к.т.н.");
        Person pp2 = new Person(null, "Ректор");
        List<Person> find1 = p.select_params(pp1);
        assertEquals(2, find1.size());
        assertEquals(p1, find1.get(0));
        assertEquals(p3, find1.get(1));
        List<Person> find2 = p.select_params(pp2);
        assertEquals(0, find2.size());
    }
    public void testInsert() throws ParseException{
        Persons p = new Persons_Gateway();
        String []fio1 = {"Спиридонов","Вадим","Юрьевич"};
        Person pp1 = new Person(fio1, "Заместитель ректора", null , null, "к.т.н.");
        String []fio2 = {"Яблоков","Алексей","Николаевич"};
        Person pp2 = new Person(fio2, "Старший преподаватель", "A", null, null);
        boolean b1 = p.insert(3, pp1);
        assertTrue(b1);
        boolean b2 = p.insert(3, pp2);
        assertFalse(b2);
    }
    public void testUpdate() throws ParseException{
        Persons p = new Persons_Gateway();
        String []fio1 = {"Яблоков","Алексей","Николаевич"};
        Person pp1 = new Person(fio1, "Старший преподаватель", "A", null, null);
        boolean b1 = p.update(3, pp1);
        assertTrue(b1);
        boolean b2 = p.update(4, pp1);
        assertFalse(b2);
    }
    public void testDelete(){
        Persons p = new Persons_Gateway();
        Person old = p.select(0);
        assertEquals(p1, old);
        boolean b1 = p.delete(0);
        assertTrue(b1);
        Person neww = p.select(0);
        assertEquals(null, neww);
        boolean b2 = p.delete(0);
        assertFalse(b2);
    }
    public void testClear(){
        Persons p = new Persons_Gateway();
        p.clear();
        assertEquals(0, p.getAll().size());
    }
    public static void main(String[] args) throws IOException, ParseException {
        TestPersons tp = new TestPersons();
        tp.init();
        tp.testSelect1();
        tp.testSelect2();
        tp.testSelectParams();
        tp.testInsert();
        tp.testUpdate();
        tp.testDelete();
        tp.testClear();
    }
}
