/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestManaging;
import ManagingClasses.*;
import ObjectClasses.*;
import java.io.*;
import java.text.ParseException;
import static junit.framework.Assert.*;
import DBClasses.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
/**
 *
 * @author justiceforall88
 */

public class TestRequestData {
    
        
    public void testGetPDParams() throws ParseException{
        String []fio = {"Пупкин","Василий","Иванович"};
        Person p1 = new Person(fio, "Заведующий кафедрой", "D", "Профессор", "д.ф.-м.н.");
        RequestData rd = new RequestData(p1);
        assertEquals(p1, rd.getPDParams());
    }         
    public void testSetPDParams() throws ParseException{
        String []fio1 = {"Пупкин","Василий","Иванович"};
        Person p1 = new Person(fio1, "Заведующий кафедрой", "D", "Профессор", "д.ф.-м.н.");
        RequestData rd = new RequestData(p1);
        String []fio2 = {"Клюквин","Андрей","Николаевич"};
        Person p2 = new Person(fio2, "Декан факультета", "A", "Доцент", "к.ф.-м.н.");
        rd.setPDParams(p2);
        assertEquals(p2, rd.getPDParams());
    }         
    public void testInputData() throws ParseException{
        String []fio1 = {"Пупкин","Василий","Иванович"};
        Person p1 = new Person(fio1, "Заведующий кафедрой", "D", "Профессор", "д.ф.-м.н.");
        RequestData rd = new RequestData();
        rd.inputData(p1);
        assertEquals(p1, rd.getPDParams());
    }
    public void testQueryToDB() throws ParseException{
        String []fio1 = {"Пупкин","Василий","Иванович"};
        Person p1 = new Person(fio1, "Заведующий кафедрой", "D", "Профессор", "д.ф.-м.н.");
        String []fio2 = {"Смирнов","Петр","Олегович"};
        Person p2 = new Person(fio2, "Преподаватель", "B");
        String []fio3 = {"Соловьев","Виктор","Карлович"};
        Person p3 = new Person(fio3, "Преподаватель", "D");
        Persons p = new Persons_Gateway();
        p.insert(0, p1);
        p.insert(1, p2);
        p.insert(2, p3);
        Person pd_find1 = new Person(null, "Преподаватель");
        Person pd_find2 = new Person(null, null, "D");
        RequestData rd1 = new RequestData();
        rd1.setPDParams(pd_find1);
        rd1.queryToDB();
        
        assertEquals(2, rd1.pd.size());
        assertEquals(p2, rd1.pd.get(0));
        assertEquals(p3, rd1.pd.get(1));
        RequestData rd2 = new RequestData();
        rd2.setPDParams(pd_find2);
        rd2.queryToDB();
        assertEquals(2, rd2.pd.size());
        assertEquals(p1, rd2.pd.get(0));
        assertEquals(p3, rd2.pd.get(1));
        
    }
    public void testOrgDataForSinEmpl() throws ParseException{
        String []fio1 = {"Пупкин","Василий","Иванович"};
        Person p1 = new Person(fio1, "Заведующий кафедрой", "D", "Профессор", "д.ф.-м.н.");
        String []fio2 = {"Смирнов","Петр","Олегович"};
        Person p2 = new Person(fio2, "Преподаватель", "B");
        String []fio3 = {"Соловьев","Виктор","Карлович"};
        Person p3 = new Person(fio3, "Преподаватель", "D");
        Persons p = new Persons_Gateway();
        p.insert(0, p1);
        p.insert(1, p2);
        p.insert(2, p3);
        Person pd_find1 = new Person(null, "Преподаватель");
        RequestData rd1 = new RequestData();
        rd1.setPDParams(pd_find1);
        rd1.queryToDB();
        Person pd_find2 = new Person(null, "Старший преподаватель");
        RequestData rd2 = new RequestData();
        rd2.setPDParams(pd_find2);
        rd2.queryToDB();
        Person pd_find3 = new Person(null, null, "B");
        RequestData rd3 = new RequestData();
        rd3.setPDParams(pd_find3);
        rd3.queryToDB();
        boolean b1 = true;
        boolean b2 = true;
        boolean b3 = true;
        List <Person> pp = new ArrayList<>();
        try{
            rd1.orgDataForSinEmpl();
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error: too much person!", e.getMessage());
        }
        assertFalse(b1);
        try{
            rd2.orgDataForSinEmpl();
        }
        catch(Error e){
            b2 = false;
            assertEquals("Error: person is not found!", e.getMessage());
        }
        assertFalse(b2);
        try{
            pp = rd3.orgDataForSinEmpl();
        }
        catch(Error e){
            b3 = false;
        }
        assertTrue(b3);
        assertEquals(1, pp.size());
        Assert.assertArrayEquals(p2.getFIO(), pp.get(0).getFIO());
        assertEquals(p2.getPost(), pp.get(0).getPost());
        assertEquals(p2.getSubdiv(), pp.get(0).getSubdiv());
        assertEquals(p2.getScRank(), pp.get(0).getScRank());
        assertEquals(p2.getScGrade(), pp.get(0).getScGrade());
        Assert.assertArrayEquals(p2.getFiles(), pp.get(0).getFiles());
        Assert.assertArrayEquals(p2.getContracts(), pp.get(0).getContracts());
    }
    public void testOrgDataForMulEmpl() throws ParseException{
        String []fio1 = {"Пупкин","Василий","Иванович"};
        Person p1 = new Person(fio1, "Заведующий кафедрой", "D", "Профессор", "д.ф.-м.н.");
        String []fio2 = {"Смирнов","Петр","Олегович"};
        Person p2 = new Person(fio2, "Преподаватель", "B");
        String []fio3 = {"Соловьев","Виктор","Карлович"};
        Person p3 = new Person(fio3, "Преподаватель", "D");
        Persons p = new Persons_Gateway();
        p.insert(0, p1);
        p.insert(1, p2);
        p.insert(2, p3);
        Person pd_find1 = new Person(null, "Преподаватель");
        RequestData rd1 = new RequestData();
        rd1.setPDParams(pd_find1);
        rd1.queryToDB();
        Person pd_find2 = new Person(null, "Старший преподаватель");
        RequestData rd2 = new RequestData();
        rd2.setPDParams(pd_find2);
        rd2.queryToDB();
        boolean b1 = true;
        boolean b2 = true;
        List <Person> pp = new ArrayList<>();
        try{
            pp = rd1.orgDataForMulEmpl();
        }
        catch(Error e){
            b1 = false;
        }
        assertTrue(b1);
        assertEquals(2, pp.size());
        Assert.assertArrayEquals(p2.getFIO(), pp.get(0).getFIO());
        assertEquals(p2.getPost(), pp.get(0).getPost());
        assertEquals(p2.getSubdiv(), pp.get(0).getSubdiv());
        assertEquals(p2.getScRank(), pp.get(0).getScRank());
        assertEquals(p2.getScGrade(), pp.get(0).getScGrade());
        Assert.assertArrayEquals(p2.getFiles(), pp.get(0).getFiles());
        Assert.assertArrayEquals(p2.getContracts(), pp.get(0).getContracts());
        Assert.assertArrayEquals(p3.getFIO(), pp.get(1).getFIO());
        assertEquals(p3.getPost(), pp.get(1).getPost());
        assertEquals(p3.getSubdiv(), pp.get(1).getSubdiv());
        assertEquals(p3.getScRank(), pp.get(1).getScRank());
        assertEquals(p3.getScGrade(), pp.get(1).getScGrade());
        Assert.assertArrayEquals(p3.getFiles(), pp.get(1).getFiles());
        Assert.assertArrayEquals(p3.getContracts(), pp.get(1).getContracts());
        try{
            rd2.orgDataForMulEmpl();
        }
        catch(Error e){
            b2 = false;
            assertEquals("Error: person is not found!", e.getMessage());
        }
        assertFalse(b2);
    }
    public static void main(String[] args) throws IOException, ParseException {
        TestRequestData trd = new TestRequestData();
        
        trd.testGetPDParams();
        trd.testSetPDParams();
        trd.testInputData(); 
        trd.testQueryToDB();
        trd.testOrgDataForSinEmpl();
        trd.testOrgDataForMulEmpl();
    }
}
