/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestManaging;
import ManagingClasses.*;
import ObjectClasses.*;
import java.io.*;
import java.text.ParseException;
import static junit.framework.Assert.*;
import DBClasses.*;

/**
 *
 * @author justiceforall88
 */
public class TestChangeData {
    
    public void testGetNFIO() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetNFIOCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNFIOCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNFIOCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNFIOCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        ChangeData cd1 = new ChangeData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        String []act = cd1.getNFIO();
        assertEquals(3, act.length);
        assertEquals("Карабанов", act[0]);
        assertEquals("Алексей", act[1]);
        assertEquals("Валерьевич", act[2]);
        
    }
    public void testSetNFIO() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testSetNFIOCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testSetNFIOCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testSetNFIOCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testSetNFIOCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        String []fio2 = {"Крайнов","Дмитрий","Александрович"};
        ChangeData cd1 = new ChangeData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        cd1.setNFIO(fio2);
        String []act = cd1.getNFIO();
        assertEquals(3, act.length);
        assertEquals("Крайнов", act[0]);
        assertEquals("Дмитрий", act[1]);
        assertEquals("Александрович", act[2]);
    }
    public void testGetNPost() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetNPostCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNPostCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNPostCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNPostCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        ChangeData cd1 = new ChangeData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        String act = cd1.getNPost();
        assertEquals("Старший преподаватель", act);
        
    }
    public void testSetNPost() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetNPostCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNPostCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNPostCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNPostCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        ChangeData cd1 = new ChangeData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        cd1.setNPost("Преподаватель");
        String act = cd1.getNPost();
        assertEquals("Преподаватель", act);
    }
    public void testGetNSubdiv() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetNPostCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNPostCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNPostCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNPostCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        ChangeData cd1 = new ChangeData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        String act = cd1.getNSubdiv();
        assertEquals("B", act);
    }
    public void testSetNSubdiv() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetNPostCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNPostCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNPostCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNPostCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        ChangeData cd1 = new ChangeData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        cd1.setNSubdiv("D");
        String act = cd1.getNSubdiv();
        assertEquals("D", act);
    }
    public void testGetNScRank() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetNPostCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNPostCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNPostCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNPostCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        ChangeData cd1 = new ChangeData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        String act = cd1.getNScRank();
        assertEquals("Доцент", act);
    }
    public void testSetNScRank() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetNPostCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNPostCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNPostCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNPostCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        ChangeData cd1 = new ChangeData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        cd1.setNScRank("Профессор");
        String act = cd1.getNScRank();
        assertEquals("Профессор", act);
    }
    public void testGetNScGrade() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetNPostCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNPostCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNPostCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNPostCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        ChangeData cd1 = new ChangeData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        String act = cd1.getNScGrade();
        assertEquals("к.т.н.", act);
    }
    public void testSetNScGrade() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetNPostCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNPostCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNPostCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNPostCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        ChangeData cd1 = new ChangeData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        cd1.setNScGrade("д.т.н.");
        String act = cd1.getNScGrade();
        assertEquals("д.т.н.", act);
    }
    public void testGetNFiles() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetNPostCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNPostCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNPostCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNPostCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        ChangeData cd1 = new ChangeData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        MyFile []act = cd1.getNFiles();
        assertEquals(2, act.length);
        assertEquals(mf3, act[0]);
        assertEquals(mf4, act[1]);
    }
    public void testSetNFiles() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetNPostCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNPostCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNPostCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNPostCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        MyFile mf5 = new MyFile("testGetNPostCD5", "Testing");
        mf5.createFile();
        MyFile mf6 = new MyFile("testGetNPostCD6", "Testing");
        mf6.createFile();
        MyFile mf7 = new MyFile("testGetNPostCD7", "Testing");
        mf7.createFile();
        MyFile []mfn = {mf5,mf6, mf7};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        ChangeData cd1 = new ChangeData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        cd1.setNFiles(mfn);
        MyFile []act = cd1.getNFiles();
        assertEquals(3, act.length);
        assertEquals(mf5, act[0]);
        assertEquals(mf6, act[1]);
        assertEquals(mf7, act[2]);
    }
    public void testGetNContrs() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetNPostCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNPostCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNPostCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNPostCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        ChangeData cd1 = new ChangeData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        Contract []act = cd1.getNContrs();
        assertEquals(2, act.length);
        assertEquals(c1, act[0]);
        assertEquals(c2, act[1]);
    }
    public void testSetNContrs() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetNPostCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNPostCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNPostCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNPostCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        MyFile mf5 = new MyFile("testGetNPostCD5", "Testing");
        mf5.createFile();
        Contract c5 = new Contract("2017-02-15", mf5);
        MyFile mf6 = new MyFile("testGetNPostCD6", "Testing");
        mf6.createFile();
        Contract c6 = new Contract("2017-08-22", mf6);
        MyFile mf7 = new MyFile("testGetNPostCD7", "Testing");
        mf7.createFile();
        Contract c7 = new Contract("2017-09-20", mf7);
        Contract []cn = {c5,c6, c7};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        ChangeData cd1 = new ChangeData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        cd1.setNContrs(cn);
        Contract []act = cd1.getNContrs();
        assertEquals(3, act.length);
        assertEquals(c5, act[0]);
        assertEquals(c6, act[1]);
        assertEquals(c7, act[2]);
        
    }
    public void testIsUserTheRight(){
        ChangeData cd1 = new ChangeData();
        User user1 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        User user2 = new User("IgorTs", "NasXz4Q", TypeUser.EMPLADM);
        User user3 = new User("PetyaP", "MdcsrDew", TypeUser.EMPLOK);
        assertTrue(cd1.isUserTheRight(user1));
        assertFalse(cd1.isUserTheRight(user2));
        assertTrue(cd1.isUserTheRight(user3));
    }
    public void testIsUserTheFullRight(){
        ChangeData cd1 = new ChangeData();
        User user1 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        User user2 = new User("IgorTs", "NasXz4Q", TypeUser.EMPLADM);
        User user3 = new User("PetyaP", "MdcsrDew", TypeUser.EMPLOK);
        assertTrue(cd1.isUserTheFullRight(user1));
        assertFalse(cd1.isUserTheFullRight(user2));
        assertFalse(cd1.isUserTheFullRight(user3));
    }
    public void testInputData() throws IOException, ParseException{
        ChangeData cd1 = new ChangeData();
        MyFile mf1 = new MyFile("testGetNPostCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNPostCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNPostCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNPostCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        cd1.inputData(fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        assertEquals(3, cd1.getNFIO().length);
        assertEquals("Карабанов", cd1.getNFIO()[0]);
        assertEquals("Алексей", cd1.getNFIO()[1]);
        assertEquals("Валерьевич", cd1.getNFIO()[2]);
        assertEquals("Старший преподаватель", cd1.getNPost());
        assertEquals("B", cd1.getNSubdiv());
        assertEquals("Доцент", cd1.getNScRank());
        assertEquals("к.т.н.", cd1.getNScGrade());
        assertEquals(2, cd1.getNFiles().length);
        assertEquals(mf3, cd1.getNFiles()[0]);
        assertEquals(mf4, cd1.getNFiles()[1]);
        assertEquals(2, cd1.getNContrs().length);
        assertEquals(c1, cd1.getNContrs()[0]);
        assertEquals(c2, cd1.getNContrs()[1]);
        
    }
    public void testFillData() throws IOException, ParseException{
        ChangeData cd1 = new ChangeData();
        User user = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        MyFile mf1 = new MyFile("testGetNPostCD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetNPostCD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetNPostCD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetNPostCD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        Person p = cd1.fillData(user, fio, "Старший преподаватель", "B", "Доцент", "к.т.н.", mf, c);
        
        assertEquals(3, p.getFIO().length);
        assertEquals("Карабанов", p.getFIO()[0]);
        assertEquals("Алексей", p.getFIO()[1]);
        assertEquals("Валерьевич", p.getFIO()[2]);
        assertEquals("Старший преподаватель", p.getPost());
        assertEquals("B", p.getSubdiv());
        assertEquals("Доцент", p.getScRank());
        assertEquals("к.т.н.", p.getScGrade());
        assertEquals(2, p.getFiles().length);
        assertEquals(mf3, p.getFiles()[0]);
        assertEquals(mf4, p.getFiles()[1]);
        assertEquals(2, p.getContracts().length);
        assertEquals(c1, p.getContracts()[0]);
        assertEquals(c2, p.getContracts()[1]);
        
    }
    
    public void testMakeChanges() throws IOException, ParseException{
        int id = 8833;
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLADM);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        MyFile mf1 = new MyFile("testGetContractsPD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetContractsPD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetContractsPD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetContractsPD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Карабанов","Алексей","Валерьевич"};
        Person p1 = new Person(fio, "Старший преподаватель", "B", null, "к.т.н.", mf, c);
        Persons p = new Persons_Gateway();
        p.insert(id, p1);
        
        MyFile mf5 = new MyFile("testGetContractsPD5", "Testing");
        mf5.createFile();
        Contract c5 = new Contract("2017-06-26", mf1);
        MyFile mf6 = new MyFile("testGetContractsPD6", "Testing");
        mf6.createFile();
        Contract c6 = new Contract("2017-08-30", mf2);
        Contract []cc = {c5,c6};
        MyFile mf7 = new MyFile("testGetContractsPD7", "Testing");
        mf7.createFile();
        MyFile mf8 = new MyFile("testGetContractsPD8", "Testing");
        mf8.createFile();
        MyFile []mff = {mf7,mf8};
        String []fio2 = {"Демидов","Юрий","Алексеевич"};
        Person p2 = new Person(fio2, "Старший преподаватель", "B", "Доцент", "к.т.н.", mff, cc);
        boolean b1 = true;
        try{
            cd.makeChanges(user1, p2,id);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.makeChanges(user2, p2,id);
        assertTrue(bb);
        
        Person act = p.select(id);
        assertEquals(p2, act);
        int id2 = 2222;
        boolean bb2 = cd.makeChanges(user2, p2,id2);
        assertFalse(bb2);
    }
    
    public void testMakeAdd() throws IOException, ParseException{
        int id = 8677;
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLOK);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        MyFile mf1 = new MyFile("testGetContractsPD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetContractsPD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetContractsPD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetContractsPD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Петров","Олег","Валерьевич"};
        Person p1 = new Person(fio, "Старший преподаватель", "B", null, "к.т.н.", mf, c);
        
        boolean b1 = true;
        try{
            cd.makeAdd(user1, p1,id);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.makeAdd(user2, p1,id);
        assertTrue(bb);
        Persons p = new Persons_Gateway();
        Person p_exp = p.select(id);
        assertEquals(p_exp, p1);
        
        boolean bb2 = cd.makeAdd(user2, p1,id);
        assertFalse(bb2);
    }
    public void testMakeDel() throws ParseException, IOException{
        int id = 3116;
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLOK);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        MyFile mf1 = new MyFile("testGetContractsPD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetContractsPD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetContractsPD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetContractsPD4", "Testing");
        mf4.createFile();
        MyFile []mf = {mf3,mf4};
        String []fio = {"Мукусев","Андрей","Петрович"};
        Person p1 = new Person(fio, "Старший преподаватель", "B", null, "к.т.н.", mf, c);
        Persons p = new Persons_Gateway();
        p.insert(id, p1);
        assertEquals(p1, p.select(id));
        boolean b1 = true;
        try{
            cd.makeDel(user1, id);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.makeDel(user2,id);
        assertTrue(bb);
        assertEquals(null, p.select(id));
        boolean bb2 = cd.makeDel(user2,id);
        assertFalse(bb2);
    }
    public void testAddPost(){
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLOK);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        boolean b1 = true;
        try{
            cd.addPost(user1, "Строитель");
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.addPost(user2, "Уборщик");
        assertTrue(bb);
        boolean bb2 = cd.addPost(user2, "Уборщик");
        assertFalse(bb2);
    }
    public void testAddSubdiv(){
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLOK);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        boolean b1 = true;
        try{
            cd.addSubdiv(user1, "Z");
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.addSubdiv(user2, "I");
        assertTrue(bb);
        boolean bb2 = cd.addSubdiv(user2, "I");
        assertFalse(bb2);
    }
    public void testAddScRank(){
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLOK);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        boolean b1 = true;
        try{
            cd.addScRank(user1, "Бакалавр");
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.addScRank(user2, "Магистр");
        assertTrue(bb);
        boolean bb2 = cd.addScRank(user2, "Магистр");
        assertFalse(bb2);
    }
    public void testAddScGrade(){
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLOK);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        boolean b1 = true;
        try{
            cd.addScGrade(user1, "д.мед.н.");
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.addScGrade(user2, "к.мед.н.");
        assertTrue(bb);
        boolean bb2 = cd.addScGrade(user2, "к.мед.н.");
        assertFalse(bb2);
    }
    public void testChangePost(){
//        private static List<String> Posts = new ArrayList<>(Arrays.asList("Ректор", "Заместитель ректора",
//        "Руководитель структурного подразделения","Начальник отдела аспирантуры",
//        "Советник при ректорате", "Ассистент", "Преподаватель", "Старший преподаватель",
//        "Доцент", "Ученый секретарь совета учреждения", "Помощник ректора",
//    "Профессор", "Заведующий кафедрой", "Заместитель заведующего кафедрой", "Декан факультета",
//    "Заместитель декана факультета", "Диспетчер факультета", "Тьютор", 
//    "Специалист по учебно-методической работе", "Учебный мастер"));
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLOK);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        List_Posts p = new List_Posts_Gateway();
        assertEquals(2, p.select("Руководитель структурного подразделения"));
        boolean b1 = true;
        try{
            cd.changePost(user1, 2, "Руководитель");
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.changePost(user2, 2, "Руководитель");
        assertTrue(bb);
        assertEquals(2, p.select("Руководитель"));
        boolean bb2 = cd.changePost(user2, 3, "Руководитель");
        assertFalse(bb2);
          
    }
    public void testChangeSubdiv(){
//        private static List<String> Subdivs = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H"));
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLOK);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        List_Subdivs p = new List_Subdivs_Gateway();
        assertEquals(5, p.select("F"));
        boolean b1 = true;
        try{
            cd.changeSubdiv(user1, 5, "Y");
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.changeSubdiv(user2, 5, "Z");
        assertTrue(bb);
        assertEquals(5, p.select("Z"));
        boolean bb2 = cd.changeSubdiv(user2, 7, "Z");
        assertFalse(bb2);
    }
    public void testChangeScRank(){
        //private static List<String> ScRanks = new ArrayList<>(Arrays.asList("Доцент", "Профессор"));
        //add("Магистр");
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLOK);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        List_ScRanks p = new List_ScRanks_Gateway();
        assertEquals(2, p.select("Магистр"));
        boolean b1 = true;
        try{
            cd.changeScRank(user1, 5, "Бак");
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.changeScRank(user2, 2, "Бакалавр");
        assertTrue(bb);
        assertEquals(2, p.select("Бакалавр"));
        boolean bb2 = cd.changeScRank(user2, 5, "Бакалавр");
        assertFalse(bb2);
    }
    public void testChangeScGrade(){
//        private static List<String> ScGrades = new ArrayList<>(Arrays.asList("к.т.н.", "д.т.н.", "к.ф.-м.н.", "д.ф.-м.н.", "к.х.н.",
//        "д.х.н.", "к.э.н.","д.э.н.","к.и.н.","д.и.н.", "к.п.н.", "д.п.н."));
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLOK);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        List_ScGrades p = new List_ScGrades_Gateway();
        assertEquals(8, p.select("к.и.н."));
        boolean b1 = true;
        try{
            cd.changeScGrade(user1, 8, "к.а.н.");
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.changeScGrade(user2, 8, "к.б.н.");
        assertTrue(bb);
        assertEquals(8, p.select("к.б.н."));
        boolean bb2 = cd.changeScGrade(user2, 8, "к.б.н.");
        assertFalse(bb2);
    }
    public void testDeletePost(){
//        private static List<String> Posts = new ArrayList<>(Arrays.asList("Ректор", "Заместитель ректора",
//        "Руководитель структурного подразделения","Начальник отдела аспирантуры",
//        "Советник при ректорате", "Ассистент", "Преподаватель", "Старший преподаватель",
//        "Доцент", "Ученый секретарь совета учреждения", "Помощник ректора",
//    "Профессор", "Заведующий кафедрой", "Заместитель заведующего кафедрой", "Декан факультета",
//    "Заместитель декана факультета", "Диспетчер факультета", "Тьютор", 
//    "Специалист по учебно-методической работе", "Учебный мастер"));
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLOK);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        List_Posts p = new List_Posts_Gateway();
        assertEquals(2, p.select("Руководитель"));
        boolean b1 = true;
        try{
            cd.deletePost(user1, 2);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.deletePost(user2, 2);
        assertTrue(bb);
        assertEquals(-1, p.select("Руководитель"));
        boolean bb2 = cd.deletePost(user2, 2);
        assertFalse(bb2);
        
    }
    public void testDeleteSubdiv(){
 //        private static List<String> Subdivs = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H"));
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLOK);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        List_Subdivs p = new List_Subdivs_Gateway();
        assertEquals(7, p.select("H"));
        boolean b1 = true;
        try{
            cd.deleteSubdiv(user1, 7);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.deleteSubdiv(user2, 7);
        assertTrue(bb);
        assertEquals(-1, p.select("H"));
        boolean bb2 = cd.deleteSubdiv(user2, 7);
        assertFalse(bb2);
    }
    public void testDeleteScRank(){
        //private static List<String> ScRanks = new ArrayList<>(Arrays.asList("Доцент", "Профессор"));
        //add("Магистр");
        //change("Бакалавр");
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLOK);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        List_ScRanks p = new List_ScRanks_Gateway();
        assertEquals(2, p.select("Бакалавр"));
        boolean b1 = true;
        try{
            cd.deleteScRank(user1, 2);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.deleteScRank(user2, 2);
        assertTrue(bb);
        assertEquals(-1, p.select("Бакалавр"));
        boolean bb2 = cd.deleteScRank(user2, 2);
        assertFalse(bb2);
    }
    public void testDeleteScGrade(){
//        private static List<String> ScGrades = new ArrayList<>(Arrays.asList("к.т.н.", "д.т.н.", "к.ф.-м.н.", "д.ф.-м.н.", "к.х.н.",
//        "д.х.н.", "к.э.н.","д.э.н.","к.и.н.","д.и.н.", "к.п.н.", "д.п.н."));
        User user1 = new User("RomanM", "MyPwD123", TypeUser.EMPLOK);
        User user2 = new User("ArtemZ", "MyPwD123", TypeUser.ADMIN);
        ChangeData cd = new ChangeData();
        List_ScGrades p = new List_ScGrades_Gateway();
        assertEquals(9, p.select("д.и.н."));
        boolean b1 = true;
        try{
            cd.deleteScGrade(user1, 9);
        }
        catch(Error e){
            b1 = false;
            assertEquals("Error with access rights", e.getMessage());
        }
        assertFalse(b1);
        boolean bb = cd.deleteScGrade(user2, 9);
        assertTrue(bb);
        assertEquals(-1, p.select("д.и.н."));
        boolean bb2 = cd.deleteScGrade(user2, 9);
        assertFalse(bb2);
    }
    
    public void testIsCorrect() throws IOException, ParseException{
        MyFile mf1 = new MyFile("testGetContractsPD1", "Testing");
        mf1.createFile();
        Contract c1 = new Contract("2017-06-25", mf1);
        MyFile mf2 = new MyFile("testGetContractsPD2", "Testing");
        mf2.createFile();
        Contract c2 = new Contract("2017-08-31", mf2);
        Contract []c = {c1,c2};
        MyFile mf3 = new MyFile("testGetContractsPD3", "Testing");
        mf3.createFile();
        MyFile mf4 = new MyFile("testGetContractsPD4", "Testing");
        mf4.createFile();
        MyFile []mf_1 = {mf3};
        MyFile []mf_2 = {mf4};
        String []fio1 = {"Булычев","Михаил","Сергеевич"};
        Person p1 = new Person(fio1, "Начальник отдела аспирантуры", null , null, "к.т.н.");
        String []fio2 = {"Янов","Александр","Сергеевич"};
        Person p2 = new Person(fio2, "Преподаватель", "D", null, null, mf_1, null);
        String []fio3 = {"Герасимов","Сергей","Николаевич"};
        Person p3 = new Person(fio3, "Доцент", "E", "Доцент", "д.х.н.", mf_2, c);
        ChangeData cd1 = new ChangeData();
        ChangeData cd2 = new ChangeData();
        ChangeData cd3 = new ChangeData();
        assertTrue(cd1.isCorrect(p1));
        assertTrue(cd2.isCorrect(p2));
        assertTrue(cd3.isCorrect(p3));
        try{
            String []fio4 = {"Абакумов"};
            Person p4 = new Person(fio4, "Преподаватель", "C");
            ChangeData cd4 = new ChangeData();
            cd4.isCorrect(p4);
        }
        catch(Error e){
            assertEquals("Incomplete Person Data: Wrong number of FIO String", e.getMessage());
        }
        try{
            String []fio5 = {"Абакумов", "Максим", "Сергеевич"};
            Person p5 = new Person(fio5, "Прораб", "C");
            ChangeData cd5 = new ChangeData();
            cd5.isCorrect(p5);
        }
        catch(Error e){
            assertEquals("Error: there is no such post", e.getMessage());
        }
        try{
            String []fio6 = {"Абакумов", "Максим", "Сергеевич"};
            Person p6 = new Person(fio6, "Преподаватель", "W");
            ChangeData cd6 = new ChangeData();
            cd6.isCorrect(p6);
        }
        catch(Error e){
            assertEquals("Error: there is no such subdiv", e.getMessage());
        }
        try{
            String []fio7 = {"Матрынов", "Евгений", "Александович"};
            Person p7 = new Person(fio7, "Заведующий кафедрой", "D", "Магистр", "к.э.н.");
            ChangeData cd7 = new ChangeData();
            cd7.isCorrect(p7);
        }
        catch(Error e){
            assertEquals("Error: there is no such scrank", e.getMessage());
        }
        try{
            String []fio8 = {"Матрынов", "Евгений", "Александович"};
            Person p8 = new Person(fio8, "Заведующий кафедрой", "D", "Профессор", "к.н.");
            ChangeData cd8 = new ChangeData();
            cd8.isCorrect(p8);
        }
        catch(Error e){
            assertEquals("Error: there is no such scgrade", e.getMessage());
        }
        try{
            MyFile mf91 = new MyFile("testChechCorrectDataPD91", "Testing");
            mf91.createFile();
            Contract c91 = new Contract("2017-06-25", mf91);
            MyFile mf92 = new MyFile("testChechCorrectDataPD92", "Testing");
            mf92.createFile();
            Contract c92 = new Contract("2017-08-31", mf92);
            MyFile mf93 = new MyFile("testChechCorrectDataPD93", "Testing");
            mf93.createFile();
            Contract c93 = new Contract("2017-10-07", mf93);
            Contract []c9 = {c91,c92,c93};
            String []fio9 = {"Спиридонов", "Вадим", "Юрьевич"};
            Person p9 = new Person(fio9, "Заведующий кафедрой", "D", "Профессор", "к.т.н.",null, c9);
            ChangeData cd9 = new ChangeData();
            cd9.isCorrect(p9);
        }
        catch(Error e){
            assertEquals("Too much of valid Contracts", e.getMessage());
        }
        try{
            MyFile mf101 = new MyFile("testChechCorrectDataPD101", "Testing");
            mf101.createFile();
            Contract c101 = new Contract("2017-05-32", mf101);
            MyFile mf102 = new MyFile("testChechCorrectDataPD102", "Testing");
            mf102.createFile();
            Contract c102 = new Contract("2017-08-31", mf102);
            Contract []c10 = {c101,c102};
            String []fio10 = {"Спиридонов", "Вадим", "Юрьевич"};
            Person p10 = new Person(fio10, "Заведующий кафедрой", "D", "Профессор", "к.т.н.",null, c10);
            ChangeData cd10 = new ChangeData();
            cd10.isCorrect(p10);
        }
        catch(Error e){
            assertEquals("Incomplete Date of Contract", e.getMessage());
        }
        try{
            MyFile mf111 = new MyFile("testChechCorrectDataPD111", "Testing");
            mf111.createFile();
            Contract c111 = new Contract("1993-05-31", mf111);
            MyFile mf112 = new MyFile("testChechCorrectDataPD112", "Testing");
            mf112.createFile();
            Contract c112 = new Contract("2017-08-31", mf112);
            Contract []c11 = {c111,c112};
            String []fio11 = {"Спиридонов", "Вадим", "Юрьевич"};
            Person p11 = new Person(fio11, "Заведующий кафедрой", "D", "Профессор", "к.т.н.",null, c11);
            ChangeData cd11 = new ChangeData();
            cd11.isCorrect(p11);
        }
        catch(Error e){
            assertEquals("Wrong Date of Contract", e.getMessage());
        }
        
    }
    
    public static void main(String[] args) throws IOException, ParseException {
        
        TestChangeData tcd = new TestChangeData();
             
        tcd.testGetNFIO();
        tcd.testSetNFIO();
        tcd.testGetNPost();
        tcd.testSetNPost();
        tcd.testGetNSubdiv();
        tcd.testSetNSubdiv();     
        tcd.testGetNScRank();
        tcd.testSetNScRank();
        tcd.testGetNScGrade();
        tcd.testSetNScGrade();
        tcd.testGetNFiles();
        tcd.testSetNFiles();   
        tcd.testGetNContrs();
        tcd.testSetNContrs();
        tcd.testIsUserTheRight();
        tcd.testIsUserTheFullRight();
        tcd.testInputData();
        tcd.testFillData();
        tcd.testMakeChanges();
        
        tcd.testMakeAdd();
        tcd.testMakeDel();
        tcd.testAddPost();
        tcd.testAddSubdiv();
        tcd.testAddScRank();
        tcd.testAddScGrade(); 
        tcd.testChangePost();
        tcd.testChangeSubdiv();
        tcd.testChangeScRank();
        tcd.testChangeScGrade();
        tcd.testDeletePost();
        tcd.testDeleteSubdiv();
        tcd.testDeleteScRank();
        tcd.testDeleteScGrade();
        
        tcd.testIsCorrect();
    }
}
