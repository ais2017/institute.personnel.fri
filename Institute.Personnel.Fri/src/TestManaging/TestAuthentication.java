/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestManaging;
import ManagingClasses.*;
import ObjectClasses.*;
import java.io.*;
import static junit.framework.Assert.*;
import DBClasses.*;
/**
 *
 * @author justiceforall88
 */
public class TestAuthentication {
    
    
    public void testGetTypeAuth(){
        Authentication auth = new Authentication(Authentication.TypeAuth.PWD);
        Authentication.TypeAuth actual = auth.getTypeAuth();
        assertEquals(Authentication.TypeAuth.PWD, actual);
    }
    public void testSetTypeAuth(){
        Authentication auth = new Authentication(Authentication.TypeAuth.PWD);
        auth.setTypeAuth(Authentication.TypeAuth.BIOM_DATA);
        Authentication.TypeAuth actual = auth.getTypeAuth();
        assertEquals(Authentication.TypeAuth.BIOM_DATA, actual);
    }
    public void testGetLogin(){
        Authentication auth = new Authentication("mat_cat43", "nSAdsr21");
        String actual = auth.getLogin();
        assertEquals("mat_cat43", actual);
    }
    public void testSetLogin(){
        Authentication auth = new Authentication("mat_cat43", "nSAdsr21");
        auth.setLogin("mat_chevy95");
        String actual = auth.getLogin();
        assertEquals("mat_chevy95", actual);
    }
    public void testGetPassword(){
        Authentication auth = new Authentication("mat_cat43", "nSAdsr21");
        String actual = auth.getPassword();
        assertEquals("nSAdsr21", actual);
    }
    public void testSetPassword(){
        Authentication auth = new Authentication("mat_cat43", "nSAdsr21");
        auth.setPassword("FseMMi4t6");
        String actual = auth.getPassword();
        assertEquals("FseMMi4t6", actual);
    }
    public void testGetBiomData() throws IOException{
        MyFile mf = new MyFile("testGetBiomDataAU1", "Testing");
        mf.createFile();
        BiomData bm = new BiomData(mf);
        Authentication auth = new Authentication("mat_cat43", bm);
        BiomData actual = auth.getBiomData();
        assertEquals(bm, actual);
    }
    public void testSetBiomData() throws IOException{
        MyFile mf = new MyFile("testSetBiomDataAU1", "Testing");
        mf.createFile();
        BiomData bm_start = new BiomData(mf);
        MyFile mf2 = new MyFile("testSetBiomDataAU2", "Testing");
        mf2.createFile();
        BiomData bm_expect = new BiomData(mf2);
        Authentication auth = new Authentication("mat_cat43", bm_start);
        auth.setBiomData(bm_expect);
        BiomData actual = auth.getBiomData();
        assertEquals(bm_expect, actual);
    }
    public void testFillDataforAuthLP(){
        Authentication auth = new Authentication(Authentication.TypeAuth.PWD);
        auth.fillDataforAuthLP("costya_zinin", "bonya96");
        String act_log = auth.getLogin();
        String act_pas = auth.getPassword();
        assertEquals("costya_zinin", act_log);
        assertEquals("bonya96", act_pas);   
    }
    public void testFillDataforAuthBD() throws IOException{
        MyFile mf = new MyFile("testFillDataforAuthBDAU1", "Testing");
        mf.createFile();
        BiomData bm = new BiomData(mf);
        Authentication auth = new Authentication(Authentication.TypeAuth.BIOM_DATA);
        auth.fillDataforAuthBD("costya_zinin", bm);
        String act_log = auth.getLogin();
        BiomData act_bmd = auth.getBiomData();
        assertEquals("costya_zinin", act_log);
        assertEquals(bm, act_bmd); 
    }
    public void testFindID() throws IOException{
        String log = "Golovkin";
        String pas = "opaopa";
        MyFile mf = new MyFile("testFindIDAU1", "Testing");
        mf.createFile();
        BiomData bm = new BiomData(mf);
        BiomData []bmd = {bm};
        TypeUser tp = TypeUser.ADMIN;
        Object []objs = {pas,bmd, tp};
        Authentication auth = new Authentication(log,pas);
        Users u = new Users_Gateway();
        u.insert(log, objs);
        //Registration.Users.put(log, objs);
        
        String log2 = "Luzhetskiy";
        String pas2 = "iamshked";
        MyFile mf2 = new MyFile("testFindIDAU2", "Testing");
        mf2.createFile();
        BiomData bm2 = new BiomData(mf2);
        BiomData []bmd2 = {bm2};
        TypeUser tp2 = TypeUser.ADMIN;
        Object []objs2 = {pas2,bmd2, tp2};
        u.insert(log2, objs2);
        //Registration.Users.put(log2, objs2);
        Authentication auth2 = new Authentication(log2,pas2);
        assertTrue(auth.findID());
        assertTrue(auth2.findID());
        
    }
    public void testGetBiomDataFromInt() throws IOException{
        Authentication auth = new Authentication("Artem","asdfgh321");
        BiomData act = auth.getBiomDataFromInt("testGetBiomDataFromInAU1", "Testing");
        assertEquals("testGetBiomDataFromInAU1", act.getFile().getName());
        assertEquals("Testing", act.getFile().getPath());
    }
    public void testIsPassMatch() throws IOException{
        String log = "Golovkin";
        String pas = "opaopa";
        MyFile mf = new MyFile("testIsPassMatchAU1", "Testing");
        mf.createFile();
        BiomData bm = new BiomData(mf);
        BiomData []bmd = {bm};
        TypeUser tp = TypeUser.ADMIN;
        Object []objs = {pas,bmd, tp};
        Users u = new Users_Gateway();
        u.insert(log, objs);
        //Registration.Users.put(log, objs);
        Authentication auth = new Authentication("Golovkin","qwerty");
        assertFalse(auth.isPassMatch());
        Authentication auth2 = new Authentication("Golovkin","opaopa");
        assertTrue(auth2.isPassMatch());
    }
    public void testIsBmdMatch() throws IOException{
        String log = "Stepanyuk";
        String pas = "opaopa";
        MyFile mf1 = new MyFile("testIsBmdMatchAU1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        MyFile mf2 = new MyFile("testIsBmdMatchAU2", "Testing");
        mf2.createFile();
        BiomData bm2 = new BiomData(mf2);
        MyFile mf3 = new MyFile("testIsBmdMatchAU3", "Testing");
        mf3.createFile();
        BiomData bm3 = new BiomData(mf3);
        BiomData []bmd = {bm1, bm2};
        Application ap = new Application();
        Registration reg = new Registration(ap);
        TypeUser tp = TypeUser.ADMIN;
        reg.fillAppl("Stepanyuk", "mypass", "mypass", bmd, "12345", tp);
        //System.out.println(bm3.getFile().getPath());
        Object []objs = {pas,bmd, tp};
        Users u = new Users_Gateway();
        u.insert(log, objs);
        //Registration.Users.put(log, objs);
        Authentication auth = new Authentication("Stepanyuk",bm3);
        assertFalse(auth.isBmdMatch());
        Authentication auth2 = new Authentication("Stepanyuk",bm2);
        assertTrue(auth2.isBmdMatch());
    }
    
    public static void main(String[] args) throws IOException {
        
        TestAuthentication tau = new TestAuthentication();
             
        tau.testGetTypeAuth();
        tau.testSetTypeAuth();
        tau.testGetLogin();
        tau.testSetLogin();
        tau.testGetPassword();
        tau.testSetPassword();
        tau.testGetBiomData();
        tau.testSetBiomData();
        tau.testFillDataforAuthLP();
        tau.testFillDataforAuthBD();
        tau.testFindID();
        tau.testGetBiomDataFromInt();
        tau.testIsPassMatch();
        tau.testIsBmdMatch();
    }
}
