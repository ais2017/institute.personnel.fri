/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestManaging;
import ManagingClasses.*;
import ObjectClasses.*;
import java.io.*;
import java.text.ParseException;
import static junit.framework.Assert.*;
import org.junit.Assert;
import DBClasses.*;

/**
 *
 * @author justiceforall88
 */

public class TestRegistration {
    public void testGetAppl(){
        Application appl = new Application();
        Registration reg = new Registration(appl);
        assertEquals(appl, reg.getAppl());
    }         
    public void testSetAppl(){
        Application appl = new Application();
        Registration reg = new Registration(appl);
        Application new_appl = new Application();
        reg.setAppl(new_appl);
        assertEquals(new_appl, reg.getAppl());
    }         
    public void testFillAppl() throws IOException{
        Application appl = new Application();
        Registration reg = new Registration(appl);
        MyFile mf1 = new MyFile("testFillApplR1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        MyFile mf2 = new MyFile("testFillApplR2", "Testing");
        mf2.createFile();
        BiomData bm2 = new BiomData(mf2);
        MyFile mf3 = new MyFile("testFillApplR3", "Testing");
        mf3.createFile();
        BiomData bm3 = new BiomData(mf3);
        BiomData []bm = {bm1, bm2, bm3};
        reg.fillAppl("ArtemZ", "justiceFA", "justiceFA", bm, "33242", TypeUser.ADMIN);
        assertEquals("ArtemZ", reg.getAppl().getLogin());
        assertEquals("justiceFA", reg.getAppl().getPassword());
        assertEquals(3, reg.getAppl().getBiomDataArr().length);
        assertEquals(bm1, reg.getAppl().getBiomDataArr()[0]);
        assertEquals(bm2, reg.getAppl().getBiomDataArr()[1]);
        assertEquals(bm3, reg.getAppl().getBiomDataArr()[2]);
                
    }         
    public void testAnswAdmin(){
        Application appl = new Application();
        Registration reg = new Registration(appl);
        boolean b = reg.answAdmin(true);
        assertTrue(b);
        b = reg.answAdmin(false);
        assertFalse(b);
    }         
    public void testIsCorrectData(){
        Application appl = new Application();
        Registration reg = new Registration(appl);
        boolean b = reg.isCorrectData(true);
        assertTrue(b);
        assertEquals(ApplStat.ACCEPTED, reg.getAppl().getStatus());
        b = reg.isCorrectData(false);
        assertFalse(b);
        assertEquals(ApplStat.REJECTED, reg.getAppl().getStatus());
        
    }         
    public void testAddNewUserToDB() throws IOException{
        Application appl = new Application();
        Registration reg = new Registration(appl);
        MyFile mf1 = new MyFile("testAddNewUserToDBR1", "Testing");
        mf1.createFile();
        BiomData bm1 = new BiomData(mf1);
        MyFile mf2 = new MyFile("testAddNewUserToDBR2", "Testing");
        mf2.createFile();
        BiomData bm2 = new BiomData(mf2);
        MyFile mf3 = new MyFile("testAddNewUserToDBR3", "Testing");
        mf3.createFile();
        BiomData bm3 = new BiomData(mf3);
        BiomData []bm = {bm1, bm2, bm3};
        reg.fillAppl("ArtemZ", "justiceFA", "justiceFA", bm, "33242", TypeUser.ADMIN);
        boolean bb = true;
        try{
            reg.introUserToSystem(false);
        }
        catch(Error e){
            bb = false;
            assertEquals("No access!", e.getMessage());
        }
        assertFalse(bb);
        Users u = new Users_Gateway();      
        Object []ob0 = u.select(reg.getAppl().getLogin());
        assertEquals(null, ob0);
        reg.introUserToSystem(true);
        Object []ob = u.select(reg.getAppl().getLogin());
        assertEquals(3, ob.length);
        assertEquals("justiceFA", (String)ob[0]);
        BiomData []accept_bm = (BiomData[]) ob[1];
        assertEquals(3, accept_bm.length);
        assertEquals(bm1, accept_bm[0]);
        assertEquals(bm2, accept_bm[1]);
        assertEquals(bm3, accept_bm[2]); 
        assertEquals(TypeUser.ADMIN, (TypeUser)ob[2]);
    }
    public static void main(String[] args) throws IOException, ParseException {
        TestRegistration tr = new TestRegistration();
        
        tr.testGetAppl();         
        tr.testSetAppl();
        tr.testFillAppl();
        tr.testAnswAdmin();
        tr.testIsCorrectData();
        tr.testAddNewUserToDB();
    }
    
    
}
